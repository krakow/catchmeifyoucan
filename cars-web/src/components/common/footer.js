import React from 'react';

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <div>
                    <strong>Cars Game 2018 </strong>  &copy; 2018
                </div>
            </div>
        )
    }
}

export default Footer