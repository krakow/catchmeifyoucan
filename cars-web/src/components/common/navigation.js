import React, {Component} from 'react';
import {Link} from 'react-router';

class Navigation extends Component {


    componentDidMount() {
        const {menu} = this.refs;
        // $(menu).metisMenu();
    }

    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }

    render() {
        return (
            <nav className="navbar-default navbar-static-side" role="navigation">
                <ul className="nav">
                    <li className="nav-header">
                        <div className="logo-element">
                            Cars Game
                        </div>
                    </li>
                    <li className={this.activeRoute("/dashboard")}>
                        <Link to="/dashboard"><i className="fa fa-th-large"></i> <span
                            className="nav-label">Dashboard</span></Link>
                    </li>
                    <li className={this.activeRoute("/cars")}>
                        <Link to="/cars"><i className="fa fa-th-large"></i> <span className="nav-label">Cars</span></Link>
                    </li>
                    <li className={this.activeRoute("/game")}>
                        <Link to="/games"><i className="fa fa-th-large"></i> <span
                            className="nav-label">Games</span></Link>
                    </li>
                    <li className={this.activeRoute("/maps")}>
                        <Link to="/maps"><i className="fa fa-th-large"></i> <span
                            className="nav-label">Maps</span></Link>
                    </li>

                </ul>

            </nav>
        )
    }
}

export default Navigation