import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types';



const getCountdownLabelClasses = (fieldValue, maxLength, customLengthFunction) => {
  return classNames({
    'field-countdown-label': true,
    'has-error': isFieldLengthGreaterThanExpected(fieldValue, maxLength, customLengthFunction)
  });
}

const isFieldLengthGreaterThanExpected = (fieldValue, maxLength, customLengthFunction) => {
  if (fieldValue)
    return getFieldLength(fieldValue, customLengthFunction) > maxLength;
}

const getFieldLength = (fieldValue, customLengthFunction) => {
  if (customLengthFunction) {
    return customLengthFunction;
  } else if (fieldValue) {
    return fieldValue.length;
  }
  return 0;
}

const InputLengthIndicator = ({fieldValue, maxLength, customLengthFunction}) =>
  <span
    className={getCountdownLabelClasses(fieldValue, maxLength, customLengthFunction)}>
    {getFieldLength(fieldValue, customLengthFunction)}
    / {maxLength}</span>;

InputLengthIndicator.propTypes = {
  fieldValue: PropTypes.arrayOf(PropTypes.string).isRequired,
  maxLength: PropTypes.arrayOf(PropTypes.string).isRequired,
  customLengthFunction: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default InputLengthIndicator;