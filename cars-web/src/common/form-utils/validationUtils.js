const validationUtils = {
  maxLength: (max, fieldName) => value => value && value.length > max ? `Please shorten your ${fieldName}` : undefined,
  validateMail: value =>
    /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
      .exec(value),
  required: (value) => value ? undefined : 'This field is required',

};
export default validationUtils;