import validationUtils from './form-utils/validationUtils';

const validators = {
  required: value => value ? undefined : 'This field is required',
  groupNameMaxLength: validationUtils.maxLength(30, 'group name'),
  groupDescriptionMaxLength: validationUtils.maxLength(250, 'description')
};

export default validators