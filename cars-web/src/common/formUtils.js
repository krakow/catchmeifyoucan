export const getFieldErrorMessage = (form, fieldName) => {
  if (form) {
    if (form.syncErrors &&
      form.fields &&
      form.fields[fieldName] &&
      form.fields[fieldName].touched &&
      form.syncErrors[fieldName]) {
      return form.syncErrors[fieldName]
    }
    if (form.asyncErrors) {
      return form.asyncErrors[fieldName]
    }
  }
};
