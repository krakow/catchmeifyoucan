const Utils = (function () {

  function extractDomain(url) {
    if (url) {
      var domain;
      if (url.indexOf('://') > -1) {
        domain = url.split('/')[2];
      } else {
        domain = url.split('/')[0];
      }
      domain = domain.split(':')[0];

      domain = domain.replace(/\/$/, '');

      if (domain.indexOf('www.') !== -1) {
        domain = domain.substring(domain.indexOf('.') + 1);
      }
      return domain;
    }
    else {
      return null;
    }
  }

  function substrWithElipsis(str, limit) {
    return str.substr(0, limit) + '...';
  }

  // add startsWith to String proto
  if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (searchString, position) {
      position = position || 0;
      return this.substr(position, searchString.length) === searchString;
    };
  }

  function bind(obj, ...methods) {
    methods.forEach(method => obj[method] = obj[method].bind(obj));
  }

  function compactArray(array) {
    return array.filter(item => !!item);
  }

  function hasSameValues(array) {
   return !array.some((value, index, array) => value !== array[0]);
  }

  return {
    extractDomain,
    substrWithElipsis,
    bind,
    compactArray,
    hasSameValues
  };
})();


export default Utils;