import T from './actionTypes';
import * as carsService from '../../services/carService';
//import {browserHistory} from 'react-router';

const fetchCars = () => ({
    type: T.CARS_DATA.FETCH_CARS
});

const fetchCarsSuccess = (car) => ({
    type: T.CARS_DATA.FETCH_CARS_SUCCESS,
    car
});

const fetchCarsFailure = (error) => ({
    type: T.CARS_DATA.FETCH_CARS_FAILURE,
    error
});


const saveCars = () => ({
    type: T.CARS_DATA.SAVE_NEW_CAR
});

const saveNewCarSuccess = (car) => ({
    type: T.CARS_DATA.SAVE_NEW_CAR_SUCCESS,
    car
});

const saveNewCarFailure = (error) => ({
    type: T.CARS_DATA.SAVE_NEW_CAR_FAILURE,
    error
});



export const getCars = () => {
    return (dispatch) => {
        dispatch(fetchCars());
        carsService.getAllCars()
            .then(fetchedCars => {
                dispatch(fetchCarsSuccess(fetchedCars));
            }).catch(error => {
            if (error.status === 401) {
                dispatch(fetchCarsFailure(error))
                //browserHistory.push('/signIn');
            } else {
                dispatch(fetchCarsFailure(error))
            }
        });

    };
};

export const saveNewCar = (data) => {
    return (dispatch) => {
        dispatch(saveCars());
        carsService.createNewCar(data)
            .then(savedCar => {
                data.id = savedCar.id;
                dispatch(saveNewCarSuccess(data));
            }).catch(error => {
            if (error.status === 401) {
                dispatch(saveNewCarFailure(error))
                //browserHistory.push('/signIn');
            } else {
                dispatch(saveNewCarFailure(error))
            }
        });

    };
};
