import React, {Component} from 'react';
import CarsTable from './carsTable';
import {connect} from 'react-redux';
import * as CarsActions from './actions';
import {bindActionCreators} from 'redux';

@connect(
    state => {
        const {cars} = state;
        return {
            cars: cars.car,
            isLoading: cars.isLoading,
            carError: cars.error
        }
    },
    dispatch => ({actions: bindActionCreators(CarsActions, dispatch)})
)
class CarsContainer extends Component {


    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.actions.getCars();
    }

    render() {

        return (
            <div>
                <div className="row wrapper border-bottom white-bg page-heading">
                    <div className="col-lg-10">
                        <h2>Cars</h2>
                    </div>
                </div>

                <CarsTable carsData={this.props.cars}
                           addNewCar={this.props.actions.saveNewCar}/>

            </div>
        );
    }
}

export default CarsContainer;