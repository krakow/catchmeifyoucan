import defineActionTypes from '../../common/defineActionTypes'

export default defineActionTypes({
    CARS_DATA: `
    FETCH_CARS
    FETCH_CARS_FAILURE
    FETCH_CARS_SUCCESS
    SAVE_NEW_CAR
    SAVE_NEW_CAR_FAILURE
    SAVE_NEW_CAR_SUCCESS
  `
});