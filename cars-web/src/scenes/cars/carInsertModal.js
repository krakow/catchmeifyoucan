import React from 'react';
import * as Actions from './actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

@connect(
    state => ({}),
    dispatch => ({actions: bindActionCreators({...Actions}, dispatch)})
)
class CarInsertModal extends React.Component {


    handleSaveBtnClick = () => {
        const {columns, onModalClose} = this.props;
        const newCar = {};
        columns.forEach((column, i) => {
            if (column.hiddenOnInsert) {
                newCar[column.field] = null;
            } else {
                newCar[column.field] = this.refs[column.field].value;
            }
        }, this);

        console.log(newCar);
        this.props.actions.saveItem(newCar);
        onModalClose();
    };

    render() {
        const {
            onModalClose,
            onSave,
            columns,
            validateState,
            ignoreEditable
        } = this.props;
        return (
            <div className='modal-content modal-inser-item'>
                <h2 style={ {color: 'red'} }>Custom Insert Modal</h2>
                <div>
                    {
                        columns.map((column, i) => {
                            const {
                                editable,
                                format,
                                field,
                                name,
                                hiddenOnInsert
                            } = column;
                            if (!hiddenOnInsert) {
                                const error = validateState[field] ?
                                    (<span className='help-block bg-danger'>{ validateState[field] }</span>) :
                                    null;
                                return (
                                    <div className='form-group' key={ field }>
                                        <label>{ name } : </label>
                                        <input ref={ field } type='text' defaultValue={ '' }/>
                                        { error }
                                    </div>
                                );
                            }
                        })
                    }
                </div>

                <div className='modal-footer'>
                    <button className='btn btn-xs btn-info' onClick={ onModalClose }>Cancel</button>
                    <button className='btn btn-xs btn-danger'
                            onClick={ () => this.handleSaveBtnClick(columns, onModalClose) }>Save
                    </button>
                </div>
            </div>
        );
    }
}

export default CarInsertModal;