import T from './actionTypes';
import typedReducers from '../../common/typeReducers';


export const defaultState = {
    car: [],
    selectedId:null,
    isLoading: true,
    error: null
};


export const carsReducer = typedReducers(T.CARS_DATA, defaultState, {
    FETCH_CARS: (state, action) => ({
        ...state,
        isLoading: true
    }),

    FETCH_CARS_SUCCESS: (state, {car}) => ({
        ...state,
        car,
        isLoading: false,
        error: null
    }),

    FETCH_CARS_FAILURE: (state, {error}) => ({
        ...state,
        error,
        isLoading: false
    }),
    SAVE_NEW_CAR: (state, action) => ({
        ...state,
        isLoading: true
    }),
    SAVE_NEW_CAR_FAILURE: (state, {error}) => ({
        ...state,
        error,
        isLoading: false
    }),
    SAVE_NEW_CAR_SUCCESS: (state, {car}) => {
        return {
            ...state,
            cars: [...state.car, car],
            isLoading: false,
            error: null
        }
    }

});

