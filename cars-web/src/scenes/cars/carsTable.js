import React, {Component} from 'react';
import {BootstrapTable, ButtonGroup, TableHeaderColumn} from 'react-bootstrap-table';
import CarInsertModal from './carInsertModal';
import Utils from '../../common/utils';


class CarsTable extends Component {


    constructor(props) {
        super(props);
        Utils.bind(this, 'createCarInsertModal', 'newCarButton', 'onRowSelect','getServerErrorMessage');
    }


    createCarInsertModal = (onModalClose, onSave, columns, validateState, ignoreEditable) => {
        const attr = {
            onModalClose, onSave, columns, validateState, ignoreEditable
        };
        return (
            <CarInsertModal { ...attr  } />
        );
    }

    getServerErrorMessage() {
        const {auctionsError} = this.props;
        if (auctionsError) {
            return <spam className="label label-danger">{auctionsError.getResponseHeader("reason")}</spam>;
        }
    }


    newCarButton = props => {
        return (
            <ButtonGroup className='my-custom-class' sizeClass='btn-group-md'>
                { props.insertBtn }
                <button type='button'
                        className={ `btn btn-primary` } onClick={this.props.addNewCar}>
                    Create New Auction
                </button>
            </ButtonGroup>
        );
    };

    onRowSelect(row, isSelected, e) {
       // this.props.updateSelectedItemId(row.id);
    }

    render() {
        const selectRow = {
            mode: 'radio',
            onSelect: this.onRowSelect,
        };

        const options = {
            btnGroup: this.newCarButton(),
            insertModal: this.createCarInsertModal
        };


        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="ibox float-e-margins">
                        <div className="ibox-title">
                            <h5>Cars: <strong></strong></h5>

                        </div>
                        <div className="ibox-content">
                            <BootstrapTable data={ this.props.carsData }
                                            options={ options }
                                            selectRow={ selectRow }
                                            insertRow>
                                <TableHeaderColumn dataField='id' isKey={ true } hiddenOnInsert={true}>Car
                                    ID</TableHeaderColumn>
                                <TableHeaderColumn dataField='name'>Car Name</TableHeaderColumn>
                                <TableHeaderColumn dataField='type'>Type</TableHeaderColumn>
                                <TableHeaderColumn dataField='carState'>Car State</TableHeaderColumn>
                            </BootstrapTable>
                        </div>
                        <span
                            className="label label-danger">{this.getServerErrorMessage()}</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default CarsTable;
