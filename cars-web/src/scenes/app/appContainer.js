import React, {Component} from 'react';
import Footer from '../../components/common/footer';
import TopHeader from '../../components/common/topHeader';
import Navigation from '../../components/common/navigation';


class AppContainer extends Component {


    constructor(props) {
        super(props);
    }


    getMainView() {
        let ignoredPathname = ['/signIn', '/signUp'];
        let footer = ignoredPathname.includes(this.props.location.pathname) ? null : <Footer/>;
        let topHeader = ignoredPathname.includes(this.props.location.pathname) ? null : <TopHeader />;
        let navigation = ignoredPathname.includes(this.props.location.pathname) ? null :
            <Navigation location={this.props.location}/>;
        let wrapperClass = "gray-bg";

        return (<div className="wrapper">
            {navigation}
            <div id="page-wrapper" className={wrapperClass}>
                {topHeader}
                {this.props.children}
                {footer}
            </div>
        </div>)

    }

    getDefault() {
        return (
            <div className="gray-bg">
                {this.props.children}
            </div>)

    }

    render() {
        const ignoredPathname = ['/signIn', '/signUp'];
        let view = ignoredPathname.includes(this.props.location.pathname) ? this.getDefault() : this.getMainView();
        return (
            <div>
                {view}
            </div>
        );
    }
}

export default AppContainer;