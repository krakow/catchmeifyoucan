// import { createDevTools } from 'redux-devtools'
// import LogMonitor from 'redux-devtools-log-monitor'
// import DockMonitor from 'redux-devtools-dock-monitor'
import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import {browserHistory, Router} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import Routes from './routes';

// const DevTools = createDevTools(
//     <DockMonitor toggleVisibilityKey="ctrl-h" changePositionKey="ctrl-q">
//         <LogMonitor theme="tomorrow" preserveScrollTop={false}/>
//     </DockMonitor>
// )


const store = configureStore({});

const history = syncHistoryWithStore(browserHistory, store);
const routes = new Routes();
render(
    <Provider store={store}>
        <Router history={history} routes={routes.getRoutes()}>
        </Router>
    </Provider>,
    document.getElementById("root")
);