const config = {
    protocol: 'http',
    host: 'localhost:8080',
    context: '/'
};

export default config;