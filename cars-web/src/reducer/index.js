import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import {reducer as formReducer} from 'redux-form';
//view
import {carsReducer} from '../scenes/cars/reducer';
// import {signUpReducer} from '../scenes/signUp/reducer';
//
// import {itemsReducer} from '../scenes/items/reducer'
// import {auctionsReducer} from '../scenes/auctions/reducer'
// import {userProfileReducer} from '../scenes/userProfile/reducer'
// import {snapshotReducer} from '../scenes/snapshot/reducer'


const appReducer = combineReducers({
    routing: routerReducer,
    cars: carsReducer
    // form: formReducer,
    // view: combineReducers({
    //     signIn: signInReducer,
    //     signUp: signUpReducer,
    // }),
    // items: itemsReducer,
    // auctions: auctionsReducer,
    // userProfile: userProfileReducer,
    // snapshot: snapshotReducer
});

export default appReducer;
