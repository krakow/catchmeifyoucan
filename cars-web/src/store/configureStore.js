import promise from 'redux-promise';
import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducer from '../reducer';
import createLogger from 'redux-logger';
import {composeWithDevTools} from 'redux-devtools-extension';

function configureStore(initialState) {

    // middleware that logs actions
    const logger = createLogger();
    const enhancer = composeWithDevTools(
        applyMiddleware(
            thunkMiddleware, // lets us dispatch() functions
            promise,
            logger,
        )
    );
    return createStore(reducer, initialState, enhancer);
}

export default function (initialState) {
    return configureStore(initialState)
};
