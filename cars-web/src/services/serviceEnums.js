export const RequestMethod = {
    POST: 'POST',
    GET: 'GET',
    PUT: 'PUT',
    DELETE: 'DELETE',
    PATCH: 'PATCH'
};

export const ContentType = {
    JSON: 'application/json; charset=utf-8',
    PLAIN_TEXT: 'text/plain; charset=utf-8',
    EXCEL_FILE: 'application/vnd.ms-excel',
};
