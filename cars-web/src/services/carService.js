import * as Endpoints from './endpoints';
import $ from 'jquery';
import {ContentType, RequestMethod} from './serviceEnums';


export const createNewCar = (car) => {
    return $.ajax({
        type: RequestMethod.POST,
        url: Endpoints.CREATE_NEW_CAR,
        contentType: ContentType.JSON,
        data: JSON.stringify(car)
    });
};


export const getAllCars = () => {
    return $.ajax({
        type: RequestMethod.GET,
        url: Endpoints.GET_CAR_ALL_CARS,
        contentType: ContentType.JSON
    });
};



export const getCarById = (carId) => {
    return $.ajax({
        type: RequestMethod.PUT,
        url: Endpoints.GET_CAR_BY_ID  + auctionId,
        contentType: ContentType.JSON
    });
};

export const deleteCarById = (id) => {
    return $.ajax({
        type: RequestMethod.GET,
        url: Endpoints.DELETE_CAR_BY_ID+id,
        contentType: ContentType.JSON
    });
};
