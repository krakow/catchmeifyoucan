import config from '../config/dev'

const API_PREFIX = '/api/v1';

const BASE_URL = `${config.protocol}://${config.host}${config.context}`;
const API_URL = `${BASE_URL}${API_PREFIX}`;


// cars
export const GET_CAR_ALL_CARS = `${API_URL}/cars`;
export const GET_CAR_BY_ID = `${API_URL}/cars/`;
export const CREATE_NEW_CAR = `${API_URL}/cars`;
export const DELETE_CAR_BY_ID = `${API_URL}/cars`;
export const REPAIR_CAR_BY_ID = `${API_URL}/cars/{id}repair`;
export const RENT_CAR_BY_ID = `${API_URL}/cars/{id}/rent`;
export const RETUN_CAR_BY_ID = `${API_URL}/cars/{id}/return`;


//maps
export const ADD_NEW_MAP = `${API_URL}/maps/import`;
export const DELETE_MAP_BY_NAME = `${API_URL}/maps/`;


//game
export const START_NEW_GAME = `${API_URL}/games`;
export const GET_ALL_GAMES = `${API_URL}/games`;