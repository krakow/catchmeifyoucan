import * as Endpoints from './endpoints';
import $ from 'jquery';
import {ContentType, RequestMethod} from './serviceEnums';


export const startNewGame = (mapId) => {
    return $.ajax({
        type: RequestMethod.POST,
        url: Endpoints.CREATE_NEW_CAR + mapId
    });
};

export const getAllGames = () => {
    return $.ajax({
        type: RequestMethod.GET,
        url: Endpoints.DELETE_MAP_BY_NAME+name,
        contentType: ContentType.JSON
    });
};
