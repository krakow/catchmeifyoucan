import $ from 'jquery';

export const enableAuthorizationHeader = (loginData) => {
    // axios.defaults.headers.common['X-AUTH-TOKEN'] = loginData.headers['x-auth-token'];
    $.ajaxSetup({
        beforeSend(xhr) {
            if (loginData.headers['x-auth-token']) {
                xhr.setRequestHeader('x-auth-token', loginData.headers['x-auth-token']);
            }
        }
    })
};

export const disableAuthorizationHeader = () => {
    //axios.defaults.headers.common=[];
    $.ajaxSetup({
        beforeSend(xhr) {
        }
    });
};

