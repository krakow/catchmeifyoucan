import $ from 'jquery';


export const setObject = (key, dataObj) => {
    const def = $.Deferred();
    localStorage.setItem(key, dataObj);
    def.resolve(dataObj);
    return def.promise();
};

export const removeObject = (key) => {
    const def = $.Deferred();
    localStorage.removeItem(key);
    return def.promise();
};

export const getObject = (key) => {
    const def = $.Deferred();
    const data = localStorage.getItem(key);
    if (data !== null) {
        def.resolve(data);
    } else {
        def.reject(`value for ${key} not found`);
    }
    return def.promise();
};


