import * as Endpoints from './endpoints';
import $ from 'jquery';
import {ContentType, RequestMethod} from './serviceEnums';


export const createNewCar = (name,file) => {
    return $.ajax({
        type: RequestMethod.POST,
        url: Endpoints.CREATE_NEW_CAR + '?name=' + name,
        contentType: ContentType.EXCEL_FILE,
        data: file
    });
};

export const deleteCarByName = (name) => {
    return $.ajax({
        type: RequestMethod.GET,
        url: Endpoints.DELETE_MAP_BY_NAME+name,
        contentType: ContentType.JSON
    });
};
