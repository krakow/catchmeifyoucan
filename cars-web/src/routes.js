import React from 'react';
import {IndexRoute, Route} from 'react-router';
/* containers */
import AppContainer from './scenes/app/appContainer';
import DashboardContainer from './scenes/dashboard/dashboardContainer';
import MapsContainer from './scenes/maps/mapsContainer';
import GamesContainer from './scenes/games/gameContainer';
import CarsContainer from './scenes/cars/carsContainer';
import Error404 from './scenes/404/Error404';

import * as LocalStorageService from './services/localStorageService';

export default class Routes {

    constructor() {
    }

    // requireAuth(nextState, replace) {  //this is very simple solution just for demo  :-)
    //     LocalStorageService.getObject('isLoggedIn').done((data) => {
    //         let isLoggedIn = (data === 'true');
    //         if (!isLoggedIn) {
    //             return replace({
    //                 pathname: '/signIn'
    //             })
    //         }
    //     }).fail(()=>{
    //         return replace({
    //             pathname: '/signIn'
    //         })
    //     })
    //
    // }

    getRoutes() {

        return <Route path="/" component={AppContainer}>
            <IndexRoute components={DashboardContainer}/>
            <Route path="dashboard" component={DashboardContainer}/>
            <Route path="cars" component={CarsContainer} />
            <Route path="games" component={GamesContainer} />
            <Route path="maps" component={MapsContainer} />
            <Route path="*" component={Error404}/>
        </Route>
    }

}
