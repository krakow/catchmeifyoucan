# Installation instructions

1. Install yarn globally: `npm install -g yarn`

2. Install npm dependencies: `yarn install`