#!/bin/sh

echo "Start docker-machine"
docker-machine start catchMeIfYouCan

echo "Setup environment"
eval $(docker-machine env catchMeIfYouCan)

echo "Startup Tools"
docker-compose up -d