# Introduction

Necessary tools to run the app.

## Tools

| *Tool* | *Link* | *Credentials* |
| ------------- | ------------- | ------------- |
| Kafka  | http://${docker-machine ip catchMeIfYouCan}:9092/ | - |
| Mysql | http://${docker-machine ip catchMeIfYouCan}:3306/ | root/secret |
| Adminer | http://${docker-machine ip catchMeIfYouCan}:7777/ | root/secret |
| Swagger UI | http://localhost:8080/swagger-ui.html | |

## Prerequisites (Mac)

You should have Docker Toolbox installed, see https://docs.docker.com/machine/install-machine/

### Step 1 - Create Docker Machine 

Run below command if You don't have existing catchMeIfYouCan docker machine. 

```
docker-machine create -d virtualbox --virtualbox-memory "8192" catchMeIfYouCan

echo "Setup connection to machine"
eval $(docker-machine env catchMeIfYouCan)
```

### Step 2 - Setup Kafka KAFKA_ADVERTISED_HOST_NAME in docker-compose.yml

Change environment property to point to your catchMeIfYouCan docker-machine e.g:
kafka.environment.KAFKA_ADVERTISED_HOST_NAME: <YOUR_DOCKER_MACHINE_IP>

In order to get the ip address of the catchMeIfYouCan machine execute below cmd:
```
docker-machine ip catchMeIfYouCan
```      

### Step 3 - Start machine and Run Docker Compose

Run below command to start all backend tools.

```
docker-compose up -d
```

### Step 4 - Run Server Application with docker profile on

| *Tool* | *Description* |
| ------------- | ------------- |
| Gradle | ./gradlew runInDocker |


# Useful Docker-machine commands

| *Command* | *Description* | 
| ------------- | ------------- | 
| docker-machine ip catchMeIfYouCan | check ip of catchMeIfYouCan machine | 
| docker-machine start catchMeIfYouCan | start catchMeIfYouCan machine | 
| eval $(docker-machine env catchMeIfYouCan) | setup env variables to connect to docker-machine deamon | 

# Useful docker-compose commands

| *Command* | *Description* | 
| ------------- | ------------- | 
| docker-compose up -d  | start docker services in detached mode | 
| docker-compose logs <serviceName>  | show logs for specified service | 
| docker-compose logs -f  | show logs and continue streaming the new output from the container’s STDOUT and STDERR | 

