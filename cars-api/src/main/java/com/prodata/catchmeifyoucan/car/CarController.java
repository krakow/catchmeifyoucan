package com.prodata.catchmeifyoucan.car;

import com.prodata.catchmeifyoucan.car.converters.CarStateEnumConverter;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/v1/cars")
@AllArgsConstructor
public class CarController {
    private final CarService carService;

    @GetMapping
    public ResponseEntity<List<CarDTO>> findAll(@RequestParam("state") CarState carState) {
        log.info("Searching for car by state {} ", carState);
        return ResponseEntity.ok(carService.findAllCars(carState));
    }

    @GetMapping("/{id}")
    public ResponseEntity<CarDTO> findById(@PathVariable Long id) {
        log.info("Searching for car by id {} ", id);
        return ResponseEntity.ok(carService.findByIdAsDTO(id));
    }

    @PostMapping
    public ResponseEntity<CarDTO> create(@RequestBody CarDTO car) {
        log.info("Creating {} car with name {}", car.getType(), car.getName());
        CarDTO newCar = carService.addCar(car);
        return ResponseEntity.ok(newCar);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        log.info("Delete car by id {} ", id);
        carService.removeCar(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("{id}/repair")
    public ResponseEntity repairCarById(@PathVariable Long id) {
        log.info("Repairing car by id {} ", id);
        carService.repairCar(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("{id}/rent")
    public ResponseEntity rentCar(@PathVariable Long id) {
        log.info("Rent car by id {} ", id);
        carService.rentCar(id);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping("{id}/return")
    public ResponseEntity returnCar(@PathVariable Long id) {
        log.info("Returning car by id {} ", id);
        carService.returnCar(id);
        return ResponseEntity.noContent().build();
    }

    @InitBinder
    public void initBinder(final WebDataBinder webDataBinder){
        webDataBinder.registerCustomEditor(CarState.class, new CarStateEnumConverter());
    }
}

