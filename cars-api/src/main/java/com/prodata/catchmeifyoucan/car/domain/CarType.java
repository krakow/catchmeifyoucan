package com.prodata.catchmeifyoucan.car.domain;

public enum CarType {
    NORMAL,
    MONSTER_TRUCK,
    RACER
}
