package com.prodata.catchmeifyoucan.car;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import com.prodata.catchmeifyoucan.car.domain.Car;
import com.prodata.catchmeifyoucan.car.domain.CarState;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    @Query("SELECT new com.prodata.catchmeifyoucan.car.CarDTO(c.id, c.name, c.type, c.carState) FROM Car c WHERE c.id = :id")
    Optional<CarDTO> findByIdAsDTO(@Param("id") Long id);

    @Query("SELECT new com.prodata.catchmeifyoucan.car.CarDTO(c.id, c.name, c.type, c.carState) FROM Car c WHERE c.carState = :carState")
    List<CarDTO> findAllByCarStateAsDTO(@Param("carState") CarState carState);

    Optional<Car> findByIdAndCarState(Long id, CarState carState);

    boolean existsByIdAndCarStateNot(Long aLong, CarState carState);
}