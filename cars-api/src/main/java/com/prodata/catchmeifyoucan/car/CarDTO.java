package com.prodata.catchmeifyoucan.car;


import com.prodata.catchmeifyoucan.car.domain.Car;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.car.domain.CarType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CarDTO {
	private Long id;
	private String name;
	private CarType type;
	private CarState carState;

	CarDTO(Car car) {
		this.id = car.getId();
		this.name = car.getName();
		this.type = car.getType();
		this.carState = car.getCarState();
	}
}
