package com.prodata.catchmeifyoucan.car.converters;

import java.beans.PropertyEditorSupport;

import com.prodata.catchmeifyoucan.car.domain.CarState;

public class CarStateEnumConverter extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		String capitalized = text.toUpperCase();
		CarState carState = CarState.valueOf(capitalized);
		setValue(carState);
	}
}
