package com.prodata.catchmeifyoucan.car.domain;

public enum CarState {
    AVAILABLE,
    RENTED,
    CRASHED
}
