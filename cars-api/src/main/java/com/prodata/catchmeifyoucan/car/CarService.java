package com.prodata.catchmeifyoucan.car;

import static com.prodata.catchmeifyoucan.car.domain.CarState.AVAILABLE;
import static com.prodata.catchmeifyoucan.car.domain.CarState.CRASHED;
import static com.prodata.catchmeifyoucan.car.domain.CarState.RENTED;

import com.prodata.catchmeifyoucan.car.domain.Car;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.exception.IllegalCarStateException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import javax.persistence.EntityNotFoundException;

@Slf4j
@Service
@AllArgsConstructor
public class CarService {
    private static final String ILLEGAL_CAR_STATE_ERROR_MESSAGE = "Car with id %s is not in %s state or car not found";
    private static final String CAR_NOT_FOUND_MESSAGE = "Car with id %s does not exist";

    private final CarRepository carRepository;

    public void removeCar(long id) {
        log.info("Removing a car with id {}", id);
        assertCanBeRemoved(id);
        carRepository.deleteById(id);
    }

    public void repairCar(long id) {
        log.info("Repairing a car with id {}", id);
        Car car = carRepository.findByIdAndCarState(id, CRASHED).orElseThrow(illegalCarStateEx(id, CRASHED));
        car.setCarState(AVAILABLE);
        carRepository.save(car);
    }

    public CarDTO addCar(CarDTO carDTO) {
        Car car = Car.builder()
                .name(carDTO.getName())
                .type(carDTO.getType())
                .carState(AVAILABLE)
                .build();
        return new CarDTO(carRepository.save(car));
    }

    public void rentCar(long id) {
        log.info("Renting a car with id {}", id);
        Car carToRent = carRepository.findByIdAndCarState(id, AVAILABLE).orElseThrow(illegalCarStateEx(id, AVAILABLE));
        carToRent.setCarState(RENTED);
        carRepository.save(carToRent);
    }

    public void returnCar(long id) {
        log.info("Returning a car with id {}", id);
        Car carToRealise = carRepository.findByIdAndCarState(id, RENTED).orElseThrow(illegalCarStateEx(id, RENTED));
        carToRealise.setCarState(AVAILABLE);
        carRepository.save(carToRealise);
    }

    public List<CarDTO> findAllCars(CarState carState) {
        return carRepository.findAllByCarStateAsDTO(carState);
    }

    public CarDTO findByIdAsDTO(Long id) {
        return carRepository.findByIdAsDTO(id).orElseThrow(() -> new EntityNotFoundException(String.format(CAR_NOT_FOUND_MESSAGE, id)));
    }

    public Car findById(Long id) {
        return carRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format(CAR_NOT_FOUND_MESSAGE, id)));
    }

    public List<Car> updateCarsState(Set<Long> ids, CarState state) {
        List<Car> cars = carRepository.findAllById(ids);
        cars.forEach(car -> car.setCarState(state));
        return carRepository.saveAll(cars);
    }

    private void assertCanBeRemoved(long id) {
        if (!carRepository.existsByIdAndCarStateNot(id, CarState.RENTED)) {
            log.error("Car with id {} does not exist or is rented", id);
            throw new EntityNotFoundException(String.format(ILLEGAL_CAR_STATE_ERROR_MESSAGE, id, CarState.RENTED));
        }
    }

    private Supplier<IllegalCarStateException> illegalCarStateEx(long id, CarState state) {
        return () -> new IllegalCarStateException(String.format(ILLEGAL_CAR_STATE_ERROR_MESSAGE, id, state));
    }
}
