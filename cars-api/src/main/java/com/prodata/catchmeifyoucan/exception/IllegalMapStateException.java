package com.prodata.catchmeifyoucan.exception;

public class IllegalMapStateException extends ApplicationException{

	public IllegalMapStateException(String message) {
		super(message);
	}
}
