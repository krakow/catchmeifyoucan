package com.prodata.catchmeifyoucan.exception;

public class MapNotValidException extends ApplicationException {

	public MapNotValidException(String message) {
		super(message);
	}
}
