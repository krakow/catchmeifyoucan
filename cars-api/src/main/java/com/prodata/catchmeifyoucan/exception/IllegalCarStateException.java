package com.prodata.catchmeifyoucan.exception;

public class IllegalCarStateException extends ApplicationException {

	public IllegalCarStateException(String message) {
		super(message);
	}
}
