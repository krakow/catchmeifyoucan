package com.prodata.catchmeifyoucan.config;

import java.io.IOException;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.prodata.catchmeifyoucan.exception.IllegalCarStateException;
import com.prodata.catchmeifyoucan.exception.MapNotValidException;
import lombok.extern.slf4j.Slf4j;

//FIXME: CHECK and DELETE not needed methods
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<String> handleException(Exception e) {
        log.error("General exception: ", e);
        return response(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ResponseEntity<String> handleEnitityNotFound(RuntimeException e) {
        log.error("Entity not found exception: ", e);
        return response(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {IllegalCarStateException.class})
    public ResponseEntity<String> handleIllegarCarState(IllegalCarStateException e) {
        log.error("IllegalCarStateException: ",e);
        return response(e.getMessage(), HttpStatus.CONFLICT);
    }


    @ExceptionHandler(value = MapNotValidException.class)
    public ResponseEntity<String> handleMapNotValidException(MapNotValidException e) {
        log.error("MapNotValidException: ", e);
        return response(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = IOException.class)
    public ResponseEntity<String> handleIOException(IOException e) {
        log.error("IO exception:", e);
        return response(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<String> response(String errorMessage, HttpStatus status) {
        return new ResponseEntity<>(errorMessage, status);
    }

}
