package com.prodata.catchmeifyoucan.config;

import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.micrometer.core.instrument.MeterRegistry;

@Configuration
public class MonitoringConfiguration {

	@Bean
	public MeterRegistryCustomizer<MeterRegistry> commonTags() {
		return registry -> registry.config().commonTags("application", "cars-api");
	}
}
