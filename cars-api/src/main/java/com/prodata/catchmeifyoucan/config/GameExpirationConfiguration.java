package com.prodata.catchmeifyoucan.config;


import com.prodata.catchmeifyoucan.config.properties.KafkaProperties;
import com.prodata.catchmeifyoucan.game.events.EndGameEvent;
import com.prodata.catchmeifyoucan.game.events.GameEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.mina.util.ExpiringMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.UUID;

@Slf4j
@Configuration
@AllArgsConstructor
public class GameExpirationConfiguration {

    private static final int GAME_INACTIVITY_TIMEOUT = 30;
    private final KafkaProperties kafkaProperties;
    private final KafkaTemplate<String, GameEvent> kafkaTemplate;

    @Bean
    public ExpiringMap<UUID, GameEvent> lastMoveEvents() {
        ExpiringMap<UUID, GameEvent> lastMoveEvents =
                new ExpiringMap<>(GAME_INACTIVITY_TIMEOUT, ExpiringMap.DEFAULT_EXPIRATION_INTERVAL);
        lastMoveEvents.getExpirer().startExpiring();
        lastMoveEvents.addExpirationListener(this::sessionExpired);
        return lastMoveEvents;
    }

    private void sessionExpired(GameEvent s) {
        log.info("Game sesion for gameId {} has expired", s.getGameId());
        kafkaTemplate.send(kafkaProperties.getGameTopic(), new EndGameEvent(s.getGameId()));
    }
}
