package com.prodata.catchmeifyoucan.config;

public interface IdGenerator {
    Long getAndIncrement();
}