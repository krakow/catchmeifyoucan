package com.prodata.catchmeifyoucan.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prodata.catchmeifyoucan.config.properties.KafkaProperties;
import com.prodata.catchmeifyoucan.game.GameAggregate;
import com.prodata.catchmeifyoucan.game.events.GameEvent;
import lombok.AllArgsConstructor;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.Consumed;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Serialized;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.support.serializer.JsonSerde;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafkaStreams
@AllArgsConstructor
public class KafkaStreamConfiguration {
    private final KafkaProperties kafkaProperties;

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    StreamsConfig streamsConfig() {
        Map<String, Object> config = new HashMap<>();
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapAddress());
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, kafkaProperties.getApplicationId());
        config.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, kafkaProperties.getStreams().getCommitIntervalMsConfig());
        config.put(StreamsConfig.POLL_MS_CONFIG, kafkaProperties.getStreams().getPollMsConfig());
        return new StreamsConfig(config);
    }

    @Bean
    KTable<String, GameAggregate> kTableGames(StreamsBuilder streamsBuilder, ObjectMapper objectMapper) {
        Serde<String> keySerde = Serdes.String();
        Serde<GameEvent> gameEventSerde = new JsonSerde<>(GameEvent.class, objectMapper);
        KStream<String, GameEvent> stream = streamsBuilder.stream(kafkaProperties.getGameTopic(), Consumed.with(keySerde, gameEventSerde));

        Serde gameAggregateSerde = new JsonSerde<>(GameAggregate.class, objectMapper);
        KTable aggregate = stream.groupByKey(Serialized.with(keySerde, gameEventSerde))
                                 .aggregate(GameAggregate::new,
                                            (s, gameEvent, game) -> game.handle(gameEvent),
                                            Materialized.as(kafkaProperties.getGameStateStore())
                                                        .withValueSerde(gameAggregateSerde)
                                                        .withKeySerde(keySerde));

        aggregate.to(keySerde, gameAggregateSerde, kafkaProperties.getGameSnapshots());
        return aggregate;
    }


}
