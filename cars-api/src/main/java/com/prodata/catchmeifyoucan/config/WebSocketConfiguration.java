package com.prodata.catchmeifyoucan.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

import com.prodata.catchmeifyoucan.config.properties.WebSocketProperties;
import lombok.AllArgsConstructor;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@AllArgsConstructor
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {
	private final WebSocketProperties webSocketProperties;

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(webSocketProperties.getEndpoint())
				.setAllowedOrigins("*") //FIXME: If we enable cors we need to modify this ;>
				.withSockJS();
	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.enableSimpleBroker(webSocketProperties.getBroker());
		registry.setApplicationDestinationPrefixes(webSocketProperties.getPrefix());
	}
}
