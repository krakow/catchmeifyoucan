package com.prodata.catchmeifyoucan.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix = "app.websocket", ignoreUnknownFields = false)
@Getter
@Setter
public class WebSocketProperties {
	private String broker;
	private String prefix;
	private String endpoint;
}
