package com.prodata.catchmeifyoucan.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix = "app.kafka", ignoreUnknownFields = false)
@Getter
@Setter
public class KafkaProperties {
	private String bootstrapAddress;
	private String groupId;
	private String auditGroupId;
	private String gameTopic;
	private String gameSnapshots;
	private String gameStateStore;
	private String applicationId;
	private Streams streams = new Streams();

	@Getter
	@Setter
	public static class Streams {
		private Integer commitIntervalMsConfig;
		private Integer pollMsConfig;
	}
}
