package com.prodata.catchmeifyoucan.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.atomic.AtomicLong;

@Configuration
@AllArgsConstructor
public class UtilsConfig {

    @Bean
    public IdGenerator playerIdGenerator() {
        //FIXME: you can replace it to use DB sequence or unique Long from UUID
        // or we can use long timestamp!
        return new IdGenerator() {
            AtomicLong atomicLong = new AtomicLong(1);
            @Override
            public Long getAndIncrement() {
                return atomicLong.getAndIncrement();
            }
        };
    }
}
