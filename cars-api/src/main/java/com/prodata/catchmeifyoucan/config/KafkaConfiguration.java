package com.prodata.catchmeifyoucan.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prodata.catchmeifyoucan.game.GameAggregate;
import com.prodata.catchmeifyoucan.game.events.GameEvent;

@Configuration
@EnableKafka
public class KafkaConfiguration {

	@Bean
	ProducerFactory<?, ?> producerFactory(KafkaProperties kafkaProperties, ObjectMapper objectMapper) {
		DefaultKafkaProducerFactory<Object, Object> producerFactory = new DefaultKafkaProducerFactory<>(kafkaProperties.buildProducerProperties());
		producerFactory.setValueSerializer(new JsonSerializer<>(objectMapper));
		return producerFactory;
	}

	@Bean
	ConsumerFactory<?, ?> gameAggregateConsumerFactory(KafkaProperties kafkaProperties, ObjectMapper objectMapper) {
		DefaultKafkaConsumerFactory<Object, Object> consumerFactory = new DefaultKafkaConsumerFactory<>(kafkaProperties.buildConsumerProperties());
		consumerFactory.setValueDeserializer(new JsonDeserializer(GameAggregate.class, objectMapper));
		return consumerFactory;
	}

	@Bean
	ConcurrentKafkaListenerContainerFactory<String, ?> gameAggregateConcurrentKafkaListenerContainerFactory(@Qualifier("gameAggregateConsumerFactory") ConsumerFactory kafkaConsumerFactory) {
		ConcurrentKafkaListenerContainerFactory<String, ?> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(kafkaConsumerFactory);
		return factory;
	}

	@Bean
	ConcurrentKafkaListenerContainerFactory<String, ?> gameAuditKafkaListenerFactory(@Qualifier("gameAggregateConsumerFactory") ConsumerFactory kafkaConsumerFactory) {
		ConcurrentKafkaListenerContainerFactory<String, ?> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(kafkaConsumerFactory);
		return factory;
	}

	@Bean
	ConsumerFactory rawConsumerFactory(KafkaProperties kafkaProperties, ObjectMapper objectMapper) {
		DefaultKafkaConsumerFactory<Object, Object> consumerFactory = new DefaultKafkaConsumerFactory<>(kafkaProperties.buildConsumerProperties());
		JsonDeserializer valueDeserializer = new JsonDeserializer<>(GameEvent.class, objectMapper);
		consumerFactory.setValueDeserializer(valueDeserializer);
		return consumerFactory;
	}

	@Bean
	ConcurrentKafkaListenerContainerFactory<String, ?> gameEventsListenerFactory(@Qualifier("rawConsumerFactory") ConsumerFactory rawConsumerFactory) {
		ConcurrentKafkaListenerContainerFactory<String, ?> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(rawConsumerFactory);
		return factory;
	}

}
