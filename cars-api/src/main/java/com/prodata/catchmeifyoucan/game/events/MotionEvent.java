package com.prodata.catchmeifyoucan.game.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.prodata.catchmeifyoucan.game.domain.Direction;
import lombok.Getter;

import java.time.Instant;
import java.util.UUID;


@Getter
public class MotionEvent extends GameEvent {
    private final String type = "game.playerMoved";

    private final Long playerId;
    private final Long carId;
//    private Point position;
    private final Direction direction;
    private final int steps;
    private final boolean autoDrive;

    @JsonCreator
    public MotionEvent(@JsonProperty("id") UUID id,
                       @JsonProperty("timestamp") Instant timestamp,
                       @JsonProperty("gameId") UUID gameId,
                       @JsonProperty("playerId") Long playerId,
                       @JsonProperty("carId") Long carId,
                       @JsonProperty("direction") Direction direction,
                       @JsonProperty("steps") int steps,
                       @JsonProperty("autoDrive") boolean autoDrive) {
        super(id, gameId, timestamp);
        this.playerId = playerId;
        this.carId = carId;
        this.direction = direction;
        this.steps = steps;
        this.autoDrive = autoDrive;
    }

    public MotionEvent(UUID gameId, Long playerId, Long carId, Direction direction, int steps, boolean autoDrive) {
        super(gameId);
        this.playerId = playerId;
        this.carId = carId;
        this.direction = direction;
        this.steps = steps;
        this.autoDrive = autoDrive;
    }

    public MotionEvent reverse() {
        return new MotionEvent(this.getGameId(),
                        this.playerId,
                        this.carId,
                        this.direction.getOppositeDirection(),
                        this.steps,
                        true);
    }
}
