package com.prodata.catchmeifyoucan.game.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class Point {
    private final Integer x;
	private final Integer y;

	@JsonCreator
	public Point(@JsonProperty("x") Integer x, @JsonProperty("y") Integer y) {
		this.x = x;
		this.y = y;
	}

	public Point move(Direction direction, int steps){
		if(steps== 0){
			return this;
		} else if (Direction.NORTH.equals(direction)) {
			return new Point(x, y-steps);
		} else if (Direction.SOUTH.equals(direction)) {
			return new Point(x, y+steps);
		} else if (Direction.EAST.equals(direction)) {
			return new Point(x+steps, y);
		} else {
			return new Point(x-steps, y);
		}
	}
}