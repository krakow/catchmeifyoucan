package com.prodata.catchmeifyoucan.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.prodata.catchmeifyoucan.game.domain.Player;
import com.prodata.catchmeifyoucan.game.events.AutopilotEndedEvent;
import com.prodata.catchmeifyoucan.game.events.AutopilotStartedEvent;
import com.prodata.catchmeifyoucan.game.events.GameEvent;
import com.prodata.catchmeifyoucan.game.events.MotionEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class AutopilotService {

	//FIXME: this still is not handling all cases additional work needed
	public List<GameEvent> generateAutopilotEvents(AutopilotStartedEvent autopilotStartedEvent, GameAggregate game) {
		UUID gameId = autopilotStartedEvent.getGameId();

		//FIXME: refactor this to complicated
		Player currentPlayer = game.getPlayers().get(autopilotStartedEvent.getPlayerId());
		List<MotionEvent> motionEvents = game.findPlayerMovements(currentPlayer.getId());
		List<MotionEvent> previousEvents = motionEvents.subList(motionEvents.size() - autopilotStartedEvent.getStepsBack(), motionEvents.size());
		previousEvents = Lists.reverse(previousEvents);

		List<MotionEvent> newEvents = previousEvents.stream()
													.map(MotionEvent::reverse)
													.collect(Collectors.toList());

		List<GameEvent> reverseEvents = new ArrayList<>();
		Stream.of(rotate180(motionEvents.get(motionEvents.size() - 1)), newEvents, rotate180(newEvents.get(newEvents.size() - 1)))
			  .forEach(reverseEvents::addAll);

		reverseEvents.add(new AutopilotEndedEvent(gameId, currentPlayer.getId()));
		return reverseEvents;
	}

	public boolean isAutopilotStartedEvent(GameEvent gameEvent) {
		return "game.autopilot_started".equals(gameEvent.getType());
	}

	private List<MotionEvent> rotate180(MotionEvent e) {
		MotionEvent firstTurn = new MotionEvent(e.getGameId(),
												e.getPlayerId(),
												e.getCarId(),
												e.getDirection().turnLeft(),
												0,
												true);

		MotionEvent secondTurnLeft = new MotionEvent(firstTurn.getGameId(),
													 firstTurn.getPlayerId(),
													 firstTurn.getCarId(),
													 firstTurn.getDirection().turnLeft(),
													 0,
													 true);


		return Arrays.asList(firstTurn, secondTurnLeft);
	}
}
