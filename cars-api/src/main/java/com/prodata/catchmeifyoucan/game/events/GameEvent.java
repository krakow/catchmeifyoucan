package com.prodata.catchmeifyoucan.game.events;


import java.time.Instant;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "type")
@JsonSubTypes({
                  @JsonSubTypes.Type(name = "game.started", value = StartGameEvent.class),
                  @JsonSubTypes.Type(name = "game.playerJoined", value = PlayerJoinedEvent.class),
                  @JsonSubTypes.Type(name = "game.playerMoved", value = MotionEvent.class),
                  @JsonSubTypes.Type(name = "game.autopilot_started", value = AutopilotStartedEvent.class),
                  @JsonSubTypes.Type(name = "game.autopilot_ended", value = AutopilotEndedEvent.class),
                  @JsonSubTypes.Type(name = "game.ended", value = EndGameEvent.class)})
public abstract class GameEvent {
    private final UUID id;
    private final UUID gameId;
    private final Instant timestamp;

    public GameEvent(UUID gameId) {
        this.id = UUID.randomUUID();
        this.timestamp = Instant.now();
        this.gameId = gameId;
    }

    public abstract String getType();
}
