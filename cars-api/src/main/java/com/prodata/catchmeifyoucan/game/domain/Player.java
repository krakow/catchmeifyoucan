package com.prodata.catchmeifyoucan.game.domain;

import com.prodata.catchmeifyoucan.car.CarDTO;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.car.domain.CarType;
import com.prodata.catchmeifyoucan.game.events.MotionEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Player {
    private Long id;
    private CarDTO car;
    private Direction direction;
    private Point position;
    private String message;
    private PlayerStatus status;

    public void carCrashed() {
        car.setCarState(CarState.CRASHED);
        status = PlayerStatus.CRASHED;
    }

    boolean drivesMonsterTruck(){
        return CarType.MONSTER_TRUCK.equals(car.getType());
    }

	public void move(MotionEvent motionEvent, Board board) {
		direction = motionEvent.getDirection();
		for (int i = 1; i <= motionEvent.getSteps(); i++) {
			position = position.move(direction, 1);
			if (board.isWall(position)) {
				this.carCrashed();
				this.setMessage(String.format("Player has hit the wall at %s position", this.getPosition()));
				break;
			}
		}
	}
}
