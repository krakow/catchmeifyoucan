package com.prodata.catchmeifyoucan.game.domain;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Sets;
import lombok.Getter;

public class Players {
	@Getter
	private Set<Player> values = Sets.newHashSet();

	public void add(Player player) {
		this.values.add(player);
	}

	public Player get(Long playerId) {
		return this.values.stream()
						  .filter(p -> p.getId().equals(playerId))
						  .findFirst()
						  .orElseThrow(() -> new IllegalStateException("Player not found " + playerId)); //FIXME: add better exception
	}

	public void resolveCarCollisions(Player currentPlayer) {
		findOthersActivePlayersOnTheSamePosition(currentPlayer).ifPresent(otherPlayer -> {
			if (collisionOfSameCarTypes(currentPlayer, otherPlayer)) {
				otherPlayer.carCrashed();
				currentPlayer.carCrashed();
			} else if (currentPlayer.drivesMonsterTruck()) {
				otherPlayer.carCrashed();
			} else {
				currentPlayer.carCrashed();
			}
		});
	}

	private Optional<Player> findOthersActivePlayersOnTheSamePosition(Player player) {
		return this.values.stream()
						  .filter(p -> !p.getId().equals(player.getId()))
						  .filter(p -> p.getStatus().isActive())
						  .filter(p -> p.getPosition().equals(player.getPosition()))
						  .findAny();
	}

	private boolean collisionOfSameCarTypes(Player currentPlayer, Player otherPlayer) {
		return otherPlayer.drivesMonsterTruck() && currentPlayer.drivesMonsterTruck() || (!otherPlayer.drivesMonsterTruck() && !currentPlayer.drivesMonsterTruck());
	}

	public Set<Long> findCrashedCarsIds() {
		return values.stream()
					 .map(player -> player.getCar().getId())
					 .collect(Collectors.toSet());
	}
}
