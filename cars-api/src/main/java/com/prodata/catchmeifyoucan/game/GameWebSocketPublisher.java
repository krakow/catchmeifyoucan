package com.prodata.catchmeifyoucan.game;

import java.time.Instant;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;

@Component
@AllArgsConstructor
@Slf4j
public class GameWebSocketPublisher {
	private SimpMessagingTemplate template;

	@KafkaListener(topics = "${app.kafka.gameSnapshots}", containerFactory = "gameAggregateConcurrentKafkaListenerContainerFactory", groupId = "${app.kafka.groupId}")
	public void listenAndPublishToWebSocket(ConsumerRecord<String, GameAggregate> cr) {
		GameAggregate game = cr.value();
		log.info("Received game aggregate {} at {}", game.getPlayers().getValues().size(), Instant.now());
		String gameId = cr.key();
		template.convertAndSend(gameChannel(gameId), game);
	}

	private String gameChannel(String gameId) {
		return String.format("/topic/game_%s", gameId);
	}
}
