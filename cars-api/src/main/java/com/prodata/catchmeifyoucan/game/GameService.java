package com.prodata.catchmeifyoucan.game;

import com.google.common.collect.Sets;
import com.prodata.catchmeifyoucan.config.properties.KafkaProperties;
import com.prodata.catchmeifyoucan.game.events.GameEvent;
import com.prodata.catchmeifyoucan.game.events.StartGameEvent;
import com.prodata.catchmeifyoucan.map.MapService;
import com.prodata.catchmeifyoucan.map.domain.GameMap;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import reactor.core.publisher.Flux;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.StreamsBuilderFactoryBean;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class GameService {
    private final MapService mapService;
    private final KafkaProperties kafkaProperties;
    private final KafkaTemplate<String, GameEvent> kafkaTemplate;
    private final StreamsBuilderFactoryBean streams;

    public Set<GameAggregate> findAll() {
        KafkaStreams kafkaStreams = streams.getKafkaStreams();
        ReadOnlyKeyValueStore<String, GameAggregate> store = kafkaStreams.store(kafkaProperties.getGameStateStore(), QueryableStoreTypes.keyValueStore());

        HashSet<GameAggregate> games = Sets.newHashSet();
        store.all()
             .forEachRemaining(record -> games.add(record.value));

        return games;
    }

    public Optional<GameAggregate> find(UUID gameId) {
        KafkaStreams kafkaStreams = streams.getKafkaStreams();
        ReadOnlyKeyValueStore<String, GameAggregate> store = kafkaStreams.store(kafkaProperties.getGameStateStore(), QueryableStoreTypes.keyValueStore());
        return Optional.ofNullable(store.get(gameId.toString()));
    }

    public UUID startNewGame(String mapName) {
        GameMap map = mapService.loadMapToGame(mapName);
        UUID gameId = UUID.randomUUID();
        GameEvent startGameEvent = new StartGameEvent(gameId, map);
        sendToGameTopic(startGameEvent);
        log.info("Started a new game with id {}", gameId);
        return gameId;
    }

    //TODO: Move to async executor (fire and forget behavior) don't block current thread. Add Spring Async
    public void sendDelayedEventsToGameTopic(List<GameEvent> gameEvents, Duration delay) {
        Flux.fromIterable(gameEvents)
            .delayElements(delay)
            .doOnNext(event -> {
                sendToGameTopic(event);
                log.info("Event {}  send to kafka topic at {}", event.getId(), Instant.now());
            }).blockLast();
    }

    public void sendToGameTopic(GameEvent gameEvent) {
        kafkaTemplate.send(kafkaProperties.getGameTopic(), gameEvent.getGameId().toString(), gameEvent);
    }
}
