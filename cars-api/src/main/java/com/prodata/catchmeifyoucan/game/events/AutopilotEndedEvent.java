package com.prodata.catchmeifyoucan.game.events;

import java.time.Instant;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;


@Getter
public class AutopilotEndedEvent extends GameEvent {
    private final String type = "game.autopilot_ended";
    private final Long playerId;

    @JsonCreator
    public AutopilotEndedEvent(@JsonProperty("id") UUID id,
							   @JsonProperty("timestamp") Instant timestamp,
							   @JsonProperty("gameId") UUID gameId,
							   @JsonProperty("playerId") Long playerId) {
        super(id, gameId, timestamp);
        this.playerId = playerId;
    }

    public AutopilotEndedEvent(UUID gameId, Long playerId) {
        super(gameId);
        this.playerId = playerId;
    }
}
