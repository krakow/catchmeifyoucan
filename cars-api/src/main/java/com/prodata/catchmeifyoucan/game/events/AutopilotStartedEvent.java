package com.prodata.catchmeifyoucan.game.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.time.Instant;
import java.util.UUID;


@Getter
public class AutopilotStartedEvent extends GameEvent {
    private final String type = "game.autopilot_started";
    private final Long playerId;
    private final int stepsBack;

    @JsonCreator
    public AutopilotStartedEvent(@JsonProperty("id") UUID id,
                                 @JsonProperty("timestamp") Instant timestamp,
                                 @JsonProperty("gameId") UUID gameId,
                                 @JsonProperty("playerId") Long playerId,
                                 @JsonProperty("stepsBack") int stepsBack) {
        super(id, gameId, timestamp);
        this.playerId = playerId;
        this.stepsBack = stepsBack;
    }

    public AutopilotStartedEvent(UUID gameId, Long playerId, int stepsBack) {
        super(gameId);
        this.playerId = playerId;
        this.stepsBack = stepsBack;
    }
}
