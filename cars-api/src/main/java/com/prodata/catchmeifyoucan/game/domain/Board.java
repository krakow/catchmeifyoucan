package com.prodata.catchmeifyoucan.game.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Board {
	private static final Integer WALL = 0;
	private Long id;
	private String name;
	private Integer[][] data;

	public boolean isWall(Point point) {
		try {
			return WALL.equals(data[point.getY()][point.getX()]);
		} catch (ArrayIndexOutOfBoundsException exception) {
			return true;
		}
	}
}
