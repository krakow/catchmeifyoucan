package com.prodata.catchmeifyoucan.game;


import com.prodata.catchmeifyoucan.game.domain.Board;
import com.prodata.catchmeifyoucan.game.domain.Player;
import com.prodata.catchmeifyoucan.game.domain.PlayerStatus;
import com.prodata.catchmeifyoucan.game.domain.Players;
import com.prodata.catchmeifyoucan.game.events.AutopilotEndedEvent;
import com.prodata.catchmeifyoucan.game.events.GameEvent;
import com.prodata.catchmeifyoucan.map.domain.GameMap;
import com.prodata.catchmeifyoucan.game.events.PlayerJoinedEvent;
import com.prodata.catchmeifyoucan.game.events.MotionEvent;
import com.prodata.catchmeifyoucan.game.events.AutopilotStartedEvent;
import com.prodata.catchmeifyoucan.game.events.StartGameEvent;
import com.prodata.catchmeifyoucan.game.events.EndGameEvent;
import javaslang.API;
import javaslang.Predicates;
import lombok.Data;
import lombok.ToString;

import java.util.*;
import java.util.stream.Collectors;

@Data
@ToString(of = {"gameId", "players", "board", "gameEnded"})
public class GameAggregate {
    private UUID gameId;
    private Players players = new Players();
    private Board board;
    private Boolean gameEnded = false;

    private final List<GameEvent> gameEvents = new ArrayList<>();

    public GameAggregate handle(GameEvent gameEvent) {
        return API.Match(gameEvent).of(
            API.Case(Predicates.instanceOf(MotionEvent.class), this::moveCar),
            API.Case(Predicates.instanceOf(AutopilotStartedEvent.class), this::autopilotStarted),
            API.Case(Predicates.instanceOf(AutopilotEndedEvent.class), this::autopilotEnded),
            API.Case(Predicates.instanceOf(StartGameEvent.class), this::startGame),
            API.Case(Predicates.instanceOf(PlayerJoinedEvent.class), this::joinGame),
            API.Case(Predicates.instanceOf(EndGameEvent.class), this::endGame));
    }

    public List<MotionEvent> findPlayerMovements(Long playerId) {
       return gameEvents.stream()
                       .filter(e -> "game.playerMoved".equals(e.getType()))
                       .map(e -> (MotionEvent) e)
                       .filter(me -> me.getPlayerId().equals(playerId))
                       .collect(Collectors.toList());
    }

    private GameAggregate startGame(StartGameEvent startEvent) {
        gameId = startEvent.getGameId();

        GameMap selectedMap = startEvent.getGameMap();
        board = new Board(selectedMap.getId(), selectedMap.getName(), selectedMap.board());
        return addEvent(startEvent);
    }

    private GameAggregate joinGame(PlayerJoinedEvent gameEvent) {
		Player player = new Player(gameEvent.getPlayerId(), gameEvent.getCar(), gameEvent.getDirection(), gameEvent.getStartingPosition(), "", PlayerStatus.DRIVING);
		players.add(player);

		if(board.isWall(player.getPosition())) {
			player.setStatus(PlayerStatus.UNABLE_TO_JOIN);
			player.setMessage("Unable to join game. Position selected is a wall. Select another starting position");
		}

		//TODO: check if position is occupied by another player if true set PlayerStatus.WAITING in auditing listener we can generate new PlayerJoinedEvent event when position will be free

        return addEvent(gameEvent);
    }

    private GameAggregate moveCar(MotionEvent motionEvent) {
        Player currentPlayer = this.players.get(motionEvent.getPlayerId());

	    currentPlayer.move(motionEvent, board);
        if (currentPlayer.getStatus().isActive()) {
	        players.resolveCarCollisions(currentPlayer);
        }
        return addEvent(motionEvent);
    }

    private GameAggregate autopilotStarted(AutopilotStartedEvent gameEvent) {
        Player currentPlayer = players.get(gameEvent.getPlayerId());
        currentPlayer.setStatus(PlayerStatus.AUTOPILOT);
        return addEvent(gameEvent);
    }

    private GameAggregate autopilotEnded(AutopilotEndedEvent gameEvent) {
        Player currentPlayer = players.get(gameEvent.getPlayerId());
        currentPlayer.setStatus(PlayerStatus.DRIVING);
        return addEvent(gameEvent);
    }

    private GameAggregate endGame(EndGameEvent endEvent) {
        if(!getGameEnded()) {
            setGameEnded(true);
            gameEvents.add(endEvent);
        }
        return this;
    }

    private GameAggregate addEvent(GameEvent gameEvent) {
        if(!getGameEnded()) {
            gameEvents.add(gameEvent);
        }
        return this;
    }
}
