package com.prodata.catchmeifyoucan.game.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.time.Instant;
import java.util.UUID;

@Getter
public class EndGameEvent extends GameEvent {
    private final String type = "game.ended";

    @JsonCreator
    public EndGameEvent(@JsonProperty("id") UUID id,
                        @JsonProperty("timestamp") Instant timestamp,
                        @JsonProperty("gameId") UUID gameId) {
        super(id, gameId, timestamp);
    }

    public EndGameEvent(UUID gameId) {
        super(gameId);
    }
}
