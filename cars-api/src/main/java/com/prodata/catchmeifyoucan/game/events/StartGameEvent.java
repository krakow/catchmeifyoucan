package com.prodata.catchmeifyoucan.game.events;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.prodata.catchmeifyoucan.map.domain.GameMap;
import lombok.Getter;

import java.time.Instant;
import java.util.UUID;

@Getter
public class StartGameEvent extends GameEvent {
    private final String type = "game.started";

    private final GameMap gameMap;

    @JsonCreator
    public StartGameEvent(@JsonProperty("id") UUID id,
                          @JsonProperty("timestamp") Instant timestamp,
                          @JsonProperty("gameId") UUID gameId,
                          @JsonProperty("gameMap") GameMap gameMap) {
        super(id, gameId, timestamp);
        this.gameMap = gameMap;
    }

    public StartGameEvent(UUID gameId, GameMap gameMap) {
        super(gameId);
        this.gameMap = gameMap;
    }
}