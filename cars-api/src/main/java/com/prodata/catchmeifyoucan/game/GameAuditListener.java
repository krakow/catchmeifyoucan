package com.prodata.catchmeifyoucan.game;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.prodata.catchmeifyoucan.car.CarService;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.game.events.GameEvent;
import com.prodata.catchmeifyoucan.game.events.AutopilotStartedEvent;
import com.prodata.catchmeifyoucan.map.MapService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;

@Slf4j
@Component
@AllArgsConstructor
public class GameAuditListener {
	private final GameExpirationSensor sensor;
	private final CarService carService;
	private final AutopilotService autopilotService;
	private final GameService gameService;
	private final MapService mapService;

	@KafkaListener(topics = "${app.kafka.gameTopic}", containerFactory = "gameEventsListenerFactory",
				   groupId = "${app.kafka.auditGroupId}")
	public void listenRawGameEvents(ConsumerRecord<String, GameEvent> cr) {
		GameEvent gameEvent = cr.value();
		log.info("Auditing received game event key {} value {}", cr.key(), gameEvent);

		//TODO: replace with commands patterns
		if(autopilotService.isAutopilotStartedEvent(gameEvent)) {
			AutopilotStartedEvent autopilotStartedEvent = (AutopilotStartedEvent) gameEvent;

			Optional<GameAggregate> game = gameService.find(gameEvent.getGameId());
			game.ifPresent(gameAggregate -> {
				List<GameEvent> autopilotEvents = autopilotService.generateAutopilotEvents(autopilotStartedEvent, gameAggregate);
				gameService.sendDelayedEventsToGameTopic(autopilotEvents, Duration.ofSeconds(1));
			});
		}
	}

	@KafkaListener(topics = "${app.kafka.gameSnapshots}", containerFactory = "gameAuditKafkaListenerFactory",
				   groupId = "${app.kafka.auditGroupId}")
	public void listenGameSnapshotTopic(ConsumerRecord<String, GameAggregate> cr) {
		GameAggregate game = cr.value();
		log.info("Auditing received game aggregate {} at {}", game.getGameId(), Instant.now());
		sensor.handleEvent(game);

		Set<Long> crashedCarsIds = game.getPlayers().findCrashedCarsIds();
		if (!crashedCarsIds.isEmpty()) {
			carService.updateCarsState(crashedCarsIds, CarState.CRASHED);
		}

		if(game.getGameEnded()) {
			 mapService.makeMapAvailable(game.getBoard().getId());
		}
	}
}
