package com.prodata.catchmeifyoucan.game;

import com.prodata.catchmeifyoucan.game.events.GameEvent;
import com.prodata.catchmeifyoucan.game.events.MotionEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.mina.util.ExpiringMap;

import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@AllArgsConstructor
public class GameExpirationSensor {
    private final ExpiringMap<UUID, GameEvent> lastMoveEvents;

    public void handleEvent(GameEvent gameEvent) {
        if(MotionEvent.class.getSimpleName().equals(gameEvent.getType())){
            lastMoveEvents.put(gameEvent.getGameId(), gameEvent);
        }
    }

    public void handleEvent(GameAggregate aggregate) {
        Optional<GameEvent> optionalLastMotionEvent = getLastMotionEvent(aggregate);

        if(gameHasNotFinishedYet(aggregate) && optionalLastMotionEvent.isPresent()) {
            handleEvent(optionalLastMotionEvent.get());
        }
    }

    private boolean gameHasNotFinishedYet(GameAggregate aggregate) {
        return !aggregate.getGameEnded();
    }

    public Optional<GameEvent> getLastMotionEvent(GameAggregate aggregate) {
        return aggregate.getGameEvents()
                .stream()
                .filter(p -> p.getType().equals(MotionEvent.class.getSimpleName()))
                .reduce((first, second) -> second);
    }
}
