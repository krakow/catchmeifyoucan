package com.prodata.catchmeifyoucan.game;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import com.prodata.catchmeifyoucan.game.events.GameEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Controller
@AllArgsConstructor
public class GameEventsController {
	private final GameService gameService;

	@MessageMapping("/games-in/")
	public void sendEvent(GameEvent event) {
		log.info("Received {} event id: {}", event.getType(), event.getId());
		gameService.sendToGameTopic(event);
	}
}
