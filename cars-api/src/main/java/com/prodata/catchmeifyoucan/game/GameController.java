package com.prodata.catchmeifyoucan.game;

import java.util.Optional;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("api/v1/games")
@AllArgsConstructor
public class GameController {
	private final GameService gameService;

	@PostMapping("start")
	public ResponseEntity startNewGame(@RequestBody StartGameDTO startGameDTO) {
		log.info("Starting new game with gameMap {}", startGameDTO.getMapId());
		UUID newGameId = gameService.startNewGame(startGameDTO.getMapId());
		return ResponseEntity.ok(newGameId.toString());
	}

	@GetMapping
	public ResponseEntity allGames() {
		return ResponseEntity.ok(gameService.findAll());
	}

	@GetMapping("{gameId}")
	public ResponseEntity getGame(@PathVariable String gameId) {
		Optional<GameAggregate> game = gameService.find(UUID.fromString(gameId));

		if (game.isPresent()) {
			return ResponseEntity.ok(game.get());
		}

		return ResponseEntity.notFound().build();
	}
}

