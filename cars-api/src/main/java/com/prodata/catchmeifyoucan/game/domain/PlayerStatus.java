package com.prodata.catchmeifyoucan.game.domain;

public enum PlayerStatus {
	AUTOPILOT,
	DRIVING,
	UNABLE_TO_JOIN,
	WAITING,
	CRASHED;

	public boolean isActive() {
		return this.equals(AUTOPILOT) || this.equals(DRIVING);
	}
}
