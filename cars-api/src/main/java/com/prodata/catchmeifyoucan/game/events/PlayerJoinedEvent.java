package com.prodata.catchmeifyoucan.game.events;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.prodata.catchmeifyoucan.car.CarDTO;
import com.prodata.catchmeifyoucan.game.domain.Direction;
import com.prodata.catchmeifyoucan.game.domain.Point;
import lombok.Getter;

import java.time.Instant;
import java.util.UUID;

@Getter
public class PlayerJoinedEvent extends GameEvent {
	private final String type = "game.playerJoined";

	private final Long playerId;
	private final CarDTO car;
	private final Point startingPosition;
	private final Direction direction;

	@JsonCreator
	public PlayerJoinedEvent(@JsonProperty("id") UUID id,
							 @JsonProperty("timestamp") Instant timestamp,
							 @JsonProperty("gameId") UUID gameId,
							 @JsonProperty("playerId") Long playerId,
							 @JsonProperty("car") CarDTO car,
							 @JsonProperty("startingPosition") Point startingPosition,
							 @JsonProperty("direction") Direction direction) {
		super(id, gameId, timestamp);
		this.playerId = playerId;
		this.car = car;
		this.startingPosition = startingPosition;
		this.direction = direction;
	}

	public PlayerJoinedEvent(UUID gameId, Long playerId, CarDTO car, Point startingPosition) {
		super(gameId);
		this.playerId = playerId;
		this.car = car;
		this.startingPosition = startingPosition;
		this.direction = Direction.NORTH;
	}
}