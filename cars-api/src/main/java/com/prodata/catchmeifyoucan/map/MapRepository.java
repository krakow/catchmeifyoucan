package com.prodata.catchmeifyoucan.map;

import com.prodata.catchmeifyoucan.map.domain.GameMap;
import com.prodata.catchmeifyoucan.map.domain.MapStatus;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface MapRepository extends JpaRepository<GameMap, Long> {

    List<GameMap> findByName(String name);

    Optional<GameMap> findByNameAndMapStatusNot(String name, MapStatus status);
    Optional<GameMap> findByNameAndMapStatusIn(String name, Set<MapStatus> status);

    boolean existsByNameAndMapStatusNot(String name, MapStatus status);
}