package com.prodata.catchmeifyoucan.map.domain;

public enum MapStatus {
	IN_GAME,
	NEVER_PLAYED,
	AVAILABLE,
	DELETED
}
