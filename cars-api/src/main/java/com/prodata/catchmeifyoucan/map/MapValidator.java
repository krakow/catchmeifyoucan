package com.prodata.catchmeifyoucan.map;

import static com.prodata.catchmeifyoucan.map.MapUtils.parseMap;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.prodata.catchmeifyoucan.exception.MapNotValidException;
import com.prodata.catchmeifyoucan.map.domain.Node;
import org.apache.commons.lang3.StringUtils;

public class MapValidator {
	private static final String MAP_IS_IMPASSABLE = "Map is impassable";
	private static final String MAP_IS_NOT_SQUARE_MATRIX = "Map is not square matrix";
	private static final String EMPTY_MAP_CONTENT_RECEIVED = "Empty mapContent received";
	private static final int WALL = 0;

	private MapValidator() { }

	public static void validateMap(String mapContent){
		if (StringUtils.isBlank(mapContent)) {
			throw new MapNotValidException(EMPTY_MAP_CONTENT_RECEIVED);
		}
		Integer[][] map = parseMap(mapContent);
		validateHasProperSize(map);
		validateRepresentsConnectedGraph(map);
	}

	private static void validateHasProperSize(Integer[][] parsedMap) {
		int numberOfRows = parsedMap.length;
		for (Integer[] row : parsedMap) {
			if (row.length != numberOfRows) {
				throw new MapNotValidException(MAP_IS_NOT_SQUARE_MATRIX);
			}
		}
	}

	private static void validateRepresentsConnectedGraph(Integer[][] map) {
		Map<Integer, Node> nodesMap = transformToNodes(map);
		ArrayDeque<Node> stack = new ArrayDeque<>();
		int connectedComponents = 0;
		for (Map.Entry<Integer, Node> integerNodeEntry : nodesMap.entrySet()) {
			Node node = integerNodeEntry.getValue();
			if (!node.isVisited()) {
				connectedComponents += 1;
				node.setVisited(true);
				stack.push(node);
				while (!stack.isEmpty()) {
					traverseConnectedNodes(nodesMap, stack);
				}
			} else if (connectedComponents > 1) {
				throw new MapNotValidException(MAP_IS_IMPASSABLE);
			}
		}
		if (connectedComponents != 1){
			throw new MapNotValidException(MAP_IS_IMPASSABLE);
		}
	}

	private static void traverseConnectedNodes(Map<Integer, Node> nodesMap, ArrayDeque<Node> stack) {
		Node v = stack.pop();
		for (Integer connectedNodeId : v.getConnectedNodes()) {
			Node neighbourNode = nodesMap.get(connectedNodeId);
			if (!neighbourNode.isVisited()) {
				neighbourNode.setVisited(true);
				stack.push(neighbourNode);
			}
		}
	}

	private static Map<Integer, Node> transformToNodes(Integer[][] array) {
		Map<Integer, Node>  nodesMap= new HashMap<>();
		int mapDimension = array.length;
		for(int i = 0; i < mapDimension; i++){
			for(int y =0; y < mapDimension; y++){
				if(array[i][y]!= WALL){
					Node node = new Node();
					Integer nodeId = calculateNodeId(i, y, mapDimension);
					node.setNodeId(nodeId);
					node.setConnectedNodes(findConnectedNodes(array, i, y));
					nodesMap.put(nodeId, node);
				}
			}
		}
		return nodesMap;
	}

	private static int calculateNodeId(int i, int y, int mapDimension) {
		return i * mapDimension + y;
	}

	private static List<Integer> findConnectedNodes(Integer[][] array, int i ,int y) {
		int matrixSize = array.length;
		List<Integer> connectedNodes = new ArrayList<>();
		if(i > 0 && array[i-1][y] != WALL){
			connectedNodes.add(calculateNodeId(i-1, y, matrixSize));
		}
		if(i < matrixSize-1 && array[i+1][y] != WALL ){
			connectedNodes.add(calculateNodeId(i+1, y, matrixSize));
		}
		if(y > 0  && array[i][y-1] != WALL){
			connectedNodes.add(calculateNodeId(i, y-1, matrixSize));

		}
		if(y < matrixSize-1 && array[i][y+1] != WALL){
			connectedNodes.add(calculateNodeId(i, y+1, matrixSize));
		}
		return connectedNodes;
	}
}
