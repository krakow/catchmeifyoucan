package com.prodata.catchmeifyoucan.map.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.prodata.catchmeifyoucan.map.MapUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "game_map")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class GameMap {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    private String map;

    @NotNull
    @Enumerated(EnumType.STRING)
    private com.prodata.catchmeifyoucan.map.domain.MapStatus mapStatus;

    public Integer[][] board(){
        return MapUtils.parseMap(map);
    }
}

