package com.prodata.catchmeifyoucan.map;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MapDTO {
    private String name;
    private String map;
}
