package com.prodata.catchmeifyoucan.map.domain;

import java.util.List;
import java.util.Objects;

import lombok.Data;

@Data
public class Node {
	Integer nodeId;
	List<Integer> connectedNodes;
	boolean visited = false;

	@Override
	public boolean equals(Object o) {
		if (this == o) { return true; }
		if (!(o instanceof Node)) { return false; }
		Node node = (Node) o;
		return Objects.equals(nodeId, node.nodeId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(nodeId);
	}
}