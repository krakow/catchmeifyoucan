package com.prodata.catchmeifyoucan.map;

import java.util.stream.Stream;

import com.prodata.catchmeifyoucan.exception.MapNotValidException;
import org.apache.commons.lang3.StringUtils;

public class MapUtils {
	private static final String COMMA_SEPARATOR = ",";
	private static final String INVALID_DELIMITER_USED = "Invalid delimiter has been used: %s";
	private static final String ALL_EOLS = "\r\n|\r|\n";

	private MapUtils() {
	}

	public static Integer[][] parseMap(String mapContent) {
		String[] rows = mapContent.split(ALL_EOLS);
		Integer[][] parsedMap = new Integer[rows.length][];
		for (int i = 0; i < rows.length; i++) {
			String[] strings = StringUtils.split(rows[i], COMMA_SEPARATOR);
			try {
				parsedMap[i] = Stream.of(strings).map(Integer::valueOf).toArray(Integer[]::new);
			} catch (NumberFormatException ex) {
				throw new MapNotValidException(String.format(INVALID_DELIMITER_USED, ex.getMessage()));
			}
		}
		return parsedMap;
	}
}
