package com.prodata.catchmeifyoucan.map;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;


@Slf4j
@RestController
@RequestMapping("api/v1/maps")
@AllArgsConstructor
@MultipartConfig(maxRequestSize = 1) //TODO: Do we need it here ??
public class MapController {
    private final MapService mapService;
    private static final String EOL = "\n";

    @PostMapping("/import")
    public ResponseEntity<MapDTO> add(@RequestParam("name") String name, @RequestParam("file") MultipartFile file) throws IOException {
        log.info("Creating gameMap with name {} and file {} with size {}",
                name, file.getOriginalFilename(), file.getSize());

        MapDTO mapDTO = mapService.createMap(name, file);
        return ResponseEntity.ok(mapDTO);
    }

    @DeleteMapping("{name}")
    public ResponseEntity delete(@PathVariable String name) {
        log.info("Delete gameMap by id {} ", name);
        mapService.deleteMap(name);
        return ResponseEntity.ok().build();
    }
}

