package com.prodata.catchmeifyoucan.map;

import static com.prodata.catchmeifyoucan.map.domain.MapStatus.DELETED;
import static com.prodata.catchmeifyoucan.map.domain.MapStatus.AVAILABLE;
import static com.prodata.catchmeifyoucan.map.domain.MapStatus.IN_GAME;
import static com.prodata.catchmeifyoucan.map.domain.MapStatus.NEVER_PLAYED;
import static org.aspectj.util.LangUtil.EOL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Sets;
import com.prodata.catchmeifyoucan.exception.IllegalMapStateException;
import com.prodata.catchmeifyoucan.exception.MapNotValidException;
import com.prodata.catchmeifyoucan.map.domain.GameMap;
import com.prodata.catchmeifyoucan.map.domain.MapStatus;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class MapService {
	private final MapRepository mapRepository;

	public void deleteMap(String mapName) {
		GameMap gameMap = mapRepository.findByNameAndMapStatusNot(mapName, MapStatus.IN_GAME).orElseThrow(() -> new IllegalMapStateException(String.format("Map with name %s is in game or not exist", mapName)));
		if (MapStatus.AVAILABLE.equals(gameMap.getMapStatus())) {
			gameMap.setMapStatus(DELETED);
			mapRepository.save(gameMap);
			log.info("Cannot delete gameMap with name {} because it was already used in game(s) - only marking as deleted", mapName);
		} else if (NEVER_PLAYED.equals(gameMap.getMapStatus())) {
			mapRepository.delete(gameMap);
		} else {
			throw new IllegalMapStateException(String.format("Map with %s id has been marked as Deleted before", mapName));
		}
	}

	public MapDTO createMap(String name, MultipartFile file) {
		assertMapNameIsAvailable(name);
		String content = parseMapFile(name, file);
		MapValidator.validateMap(content);

		GameMap gameMap = GameMap.builder()
								 .name(name)
								 .map(content)
								 .mapStatus(MapStatus.NEVER_PLAYED)
								 .build();
		GameMap savedMap = mapRepository.save(gameMap);

		return new MapDTO(savedMap.getName(), savedMap.getMap());
	}

	public GameMap loadMapToGame(String mapName) {
		GameMap gameMap = mapRepository.findByNameAndMapStatusIn(mapName, Sets.newHashSet(NEVER_PLAYED, AVAILABLE)).orElseThrow(() -> new IllegalMapStateException(String.format("Map is not available for gaming %s ", mapName)));
		gameMap.setMapStatus(IN_GAME);
		mapRepository.save(gameMap);
		return gameMap;
	}

	public GameMap makeMapAvailable(Long gameId) {
		GameMap gameMap = mapRepository.findById(gameId)
		                               .orElseThrow(() -> new EntityNotFoundException(String.format("GameMap not found for gameId %s", gameId)));
		gameMap.setMapStatus(MapStatus.AVAILABLE);
		mapRepository.save(gameMap);
		return gameMap;
	}

	private void assertMapNameIsAvailable(String name) {
		if (mapRepository.existsByNameAndMapStatusNot(name, DELETED)) {
			throw new IllegalMapStateException(String.format("The '%s' name is already used by active map", name));
		}
	}

	private String parseMapFile(String name, MultipartFile file) {
		Stream<String> lines;
		try {
			lines = new BufferedReader(new InputStreamReader(file.getInputStream())).lines();
		} catch (IOException e) {
			String fileName = file.getOriginalFilename();
			log.error("Could not read map file: {} for map: {}", fileName, name);
			throw new MapNotValidException(String.format("Invalid map file %s used for map %s", fileName, name));
		}
		return lines.collect(Collectors.joining(EOL));
	}

}
