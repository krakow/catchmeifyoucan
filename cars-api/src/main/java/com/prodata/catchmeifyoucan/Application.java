package com.prodata.catchmeifyoucan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.TimeZone;

import com.prodata.catchmeifyoucan.config.properties.KafkaProperties;
import com.prodata.catchmeifyoucan.config.properties.WebSocketProperties;

@SpringBootApplication
@EnableConfigurationProperties({KafkaProperties.class, WebSocketProperties.class})
public class Application {

    private static final String ETC_GMC = "Etc/GMT";

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    static {
        TimeZone timezone = TimeZone.getTimeZone(ETC_GMC);
        TimeZone.setDefault(timezone);
    }
}
