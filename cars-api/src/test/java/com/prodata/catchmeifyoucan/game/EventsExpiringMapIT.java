package com.prodata.catchmeifyoucan.game;

import com.prodata.catchmeifyoucan.IntegrationTest;
import com.prodata.catchmeifyoucan.config.properties.KafkaProperties;
import com.prodata.catchmeifyoucan.game.domain.Direction;
import com.prodata.catchmeifyoucan.game.events.GameEvent;
import com.prodata.catchmeifyoucan.game.events.MotionEvent;
import org.apache.mina.util.ExpiringMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@IntegrationTest
public class EventsExpiringMapIT {

    @Autowired
    ExpiringMap<UUID, GameEvent> lastMoveEvents;

    @Autowired
    KafkaProperties kafkaProperties;

    @Autowired
    KafkaTemplate<String, GameEvent> kafkaTemplate;

    @Before
    public void setUp() throws Exception {
        lastMoveEvents.setExpirationInterval(1);
        lastMoveEvents.setTimeToLive(1);
        lastMoveEvents.getExpirer().startExpiringIfNotStarted();
    }

    @Test
    public void should_handle_single_move_event_expire_and_create_end_event() throws Exception {
        lastMoveEvents.put(UUID.randomUUID(), buildSampleMoveEvent(UUID.randomUUID()));
        Thread.sleep(2000);
        assertThat(lastMoveEvents.keySet(), Matchers.empty());
    }

    private MotionEvent buildSampleMoveEvent(UUID gameId){
        return new MotionEvent(gameId, 1L, 1L, Direction.NORTH, 2, false);
    }
}