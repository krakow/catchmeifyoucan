package com.prodata.catchmeifyoucan.game.events;

import static org.assertj.core.api.Assertions.*;

import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.json.ObjectContent;

import com.prodata.catchmeifyoucan.TestUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

public class AutopilotStartedEventJsonTest {
	private JacksonTester<AutopilotStartedEvent> json;

	@Before
	public void setup() {
		JacksonTester.initFields(this, TestUtils.newObjectMapper());
	}

	//FIXME: this test should be split to two
	@Test
	public void should_serialize_and_deserialize_json() throws Exception {
		//Given
		AutopilotStartedEvent autopilotStartedEvent = new AutopilotStartedEvent(UUID.randomUUID(), 1l, 5);


		//When
		JsonContent<AutopilotStartedEvent> serialized = this.json.write(autopilotStartedEvent);
		ObjectContent<AutopilotStartedEvent> deserialized = this.json.parse(serialized.getJson());

		//Then
		assertThat(serialized)
			.hasJsonPathStringValue("@.id");

		assertThat(autopilotStartedEvent.getId()).isEqualTo(deserialized.getObject().getId());
	}
}