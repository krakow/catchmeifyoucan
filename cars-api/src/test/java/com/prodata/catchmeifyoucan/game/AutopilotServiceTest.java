package com.prodata.catchmeifyoucan.game;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.UUID;

import com.prodata.catchmeifyoucan.car.CarDTO;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.car.domain.CarType;
import com.prodata.catchmeifyoucan.game.domain.Direction;
import com.prodata.catchmeifyoucan.game.domain.Player;
import com.prodata.catchmeifyoucan.game.domain.Point;
import com.prodata.catchmeifyoucan.game.events.AutopilotStartedEvent;
import com.prodata.catchmeifyoucan.game.events.GameEvent;
import com.prodata.catchmeifyoucan.game.events.MotionEvent;
import com.prodata.catchmeifyoucan.game.events.PlayerJoinedEvent;
import com.prodata.catchmeifyoucan.game.events.StartGameEvent;
import com.prodata.catchmeifyoucan.map.domain.GameMap;
import com.prodata.catchmeifyoucan.map.domain.MapStatus;
import org.junit.Test;

//TODO: ADD more tests for reversed movement
public class AutopilotServiceTest {
	private AutopilotService cut = new AutopilotService();

	private static final String TEST_MAP =
		"0,1,0,0,0\n" +
		"0,1,0,0,0\n" +
		"0,1,1,1,1\n" +
		"0,0,0,0,1\n" +
		"1,1,1,1,1";

	private final UUID gameId = UUID.fromString("d5ea1ad6-ab0a-4959-81ca-f98e1e13ee7c");
	private final GameMap gameMap = new GameMap(1l, "test_map", TEST_MAP, MapStatus.IN_GAME);
	private final CarDTO car = newCar(1L, CarType.NORMAL);
	private final Long playerId = 1L;

	@Test
	public void should_return_3_movements_back_in_history_simple_movement() {
		//Given
		GameAggregate game = createNewGameStateWithPlayer(gameId, playerId, car, new Point(0, 4));

		Player currentPlayer = game.getPlayers().get(playerId);

		MotionEvent firstTurnRight = new MotionEvent(gameId, playerId, car.getId(), currentPlayer.getDirection().turnRight(), 0, false);
		game.handle(firstTurnRight);

		MotionEvent move1 = new MotionEvent(gameId, playerId, car.getId(), currentPlayer.getDirection(), 1, false);
		game.handle(move1);
		MotionEvent move2 = new MotionEvent(gameId, playerId, car.getId(), currentPlayer.getDirection(), 1, false);
		game.handle(move2);
		MotionEvent move3 = new MotionEvent(gameId, playerId, car.getId(), currentPlayer.getDirection(), 1, false);
		game.handle(move3);

		AutopilotStartedEvent autopilotStartedEvent = new AutopilotStartedEvent(UUID.randomUUID(), 1L, 3);

		//When
		List<GameEvent> gameEvents = cut.generateAutopilotEvents(autopilotStartedEvent, game);
		gameEvents.forEach(game::handle);

		//Then

		assertThat(currentPlayer.getDirection()).isEqualTo(Direction.EAST);
		assertThat(currentPlayer.getPosition()).isEqualTo(new Point(0, 4));
	}

	@Test
	public void should_return_3_movements_back_in_history_where_last_was_in_place_turn_right() {
		//Given
		Direction endingDirection = Direction.EAST;
		Point endingPosition = new Point(1, 4);

		GameAggregate game = createNewGameStateWithPlayer(gameId, playerId, car, new Point(0, 4));

		Player currentPlayer = game.getPlayers().get(playerId);

		MotionEvent firstTurnRight = new MotionEvent(gameId, playerId, car.getId(), currentPlayer.getDirection().turnRight(), 0, false);
		game.handle(firstTurnRight);

		MotionEvent move1 = new MotionEvent(gameId, playerId, car.getId(), currentPlayer.getDirection(), 1, false);
		game.handle(move1);
		assertThat(currentPlayer.getPosition()).isEqualTo(endingPosition);
		assertThat(currentPlayer.getDirection()).isEqualTo(endingDirection);

		MotionEvent move2 = new MotionEvent(gameId, playerId, car.getId(), currentPlayer.getDirection(), 1, false);
		game.handle(move2);
		MotionEvent move3 = new MotionEvent(gameId, playerId, car.getId(), currentPlayer.getDirection(), 1, false);
		game.handle(move3);
		MotionEvent secondTurnRight = new MotionEvent(gameId, playerId, car.getId(), currentPlayer.getDirection().turnRight(), 0, false);
		game.handle(secondTurnRight);

		AutopilotStartedEvent autopilotStartedEvent = new AutopilotStartedEvent(UUID.randomUUID(), 1L, 3);

		//When
		List<GameEvent> gameEvents = cut.generateAutopilotEvents(autopilotStartedEvent, game);
		gameEvents.forEach(game::handle);

		//Then
		assertThat(currentPlayer.getDirection()).isEqualTo(endingDirection);
		assertThat(currentPlayer.getPosition()).isEqualTo(endingPosition);
	}

	@Test
	public void should_return_true_when_autopilot_started_event_is_passed() {
		//Given
		AutopilotStartedEvent autopilotStartedEvent = new AutopilotStartedEvent(UUID.randomUUID(), 1L, 5);

		//When
		boolean result = cut.isAutopilotStartedEvent(autopilotStartedEvent);

		//Then
		assertThat(result).isTrue();
	}

	@Test
	public void should_return_false_when_start_game_event_is_passed() {
		//Given
		StartGameEvent startGameEvent = new StartGameEvent(UUID.randomUUID(), new GameMap());

		//When
		boolean result = cut.isAutopilotStartedEvent(startGameEvent);

		//Then
		assertThat(result).isFalse();
	}


	//Helpers
	private GameAggregate createNewGameState(GameMap gameMap) {
		GameAggregate game = new GameAggregate();
		StartGameEvent startGameEvent = new StartGameEvent(gameId, gameMap);
		return game.handle(startGameEvent);
	}

	private GameAggregate createNewGameStateWithPlayer(UUID gameId, Long player, CarDTO car, Point startingPosition) {
		GameAggregate game = createNewGameState(gameMap);
		PlayerJoinedEvent playerJoinedEvent = new PlayerJoinedEvent(gameId, player, car, startingPosition);
		return joinPlayers(game, playerJoinedEvent);
	}

	private GameAggregate joinPlayers(GameAggregate gameAggregate, PlayerJoinedEvent... playerJoinedEvents){
		for (PlayerJoinedEvent playerJoinedEvent : playerJoinedEvents) {
			gameAggregate.handle(playerJoinedEvent);
		}
		return gameAggregate;
	}

	private CarDTO newCar(long id, CarType carType) {
		return new CarDTO(id, "carName", carType, CarState.RENTED);
	}
}