package com.prodata.catchmeifyoucan.game;


import static org.assertj.core.api.Assertions.assertThat;

import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.json.ObjectContent;

import com.prodata.catchmeifyoucan.TestUtils;
import com.prodata.catchmeifyoucan.car.CarDTO;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.car.domain.CarType;
import com.prodata.catchmeifyoucan.game.domain.Direction;
import com.prodata.catchmeifyoucan.game.domain.Point;
import com.prodata.catchmeifyoucan.game.events.EndGameEvent;
import com.prodata.catchmeifyoucan.game.events.MotionEvent;
import com.prodata.catchmeifyoucan.game.events.PlayerJoinedEvent;
import com.prodata.catchmeifyoucan.game.events.StartGameEvent;
import com.prodata.catchmeifyoucan.map.domain.GameMap;
import com.prodata.catchmeifyoucan.map.domain.MapStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.collections.Sets;

import java.util.UUID;

public class GameAggregateJsonTest {
	private static final String TEST_MAP = "0,1,0,0,0\n0,1,0,0,0\n0,1,1,1,1\n0,0,0,0,1\n1,1,1,1,1";

	private JacksonTester<GameAggregate> json;

	@Before
	public void setup() {
		JacksonTester.initFields(this, TestUtils.newObjectMapper());
	}

	//FIXME: this test should be split to two
	@Test
	public void should_serialize_and_deserialize_json() throws Exception {
		//Given
		GameAggregate gameAggregate = new GameAggregate();
		playGame(gameAggregate);

		//When
		JsonContent<GameAggregate> serialized = this.json.write(gameAggregate);
		ObjectContent<GameAggregate> deserialized = this.json.parse(serialized.getJson());

		//Then
		assertThat(gameAggregate.getGameId()).isEqualTo(deserialized.getObject().getGameId());
	}

	private void playGame(GameAggregate gameAggregate) {
		CarDTO car = newCar(1L, CarType.MONSTER_TRUCK);
		GameMap gameMap = newGameMap();
		UUID gameId = UUID.randomUUID();
		StartGameEvent startGameEvent = new StartGameEvent(gameId, gameMap);
		PlayerJoinedEvent player1JoinedEvent = new PlayerJoinedEvent(gameId, 1l, car, new Point(0, 0));

		MotionEvent playerMotionEvent = new MotionEvent(gameId, 1l, 1l, Direction.EAST, 1, false);
		PlayerJoinedEvent returnEvent = new PlayerJoinedEvent(gameId, 1l, new CarDTO(1l, "", CarType.MONSTER_TRUCK, CarState.RENTED), new Point(1, 1));
		PlayerJoinedEvent player2JoinedEvent = new PlayerJoinedEvent(gameId, 2l, new CarDTO(2l, "", CarType.MONSTER_TRUCK, CarState.RENTED), new Point(1, 1));
		EndGameEvent event = new EndGameEvent(gameId);


		Sets.newSet(startGameEvent, player1JoinedEvent, playerMotionEvent, returnEvent, player2JoinedEvent, event)
			.forEach(gameAggregate::handle);
	}

	private GameMap newGameMap() {
		return new GameMap(1L, "TEST_MAP", TEST_MAP, MapStatus.NEVER_PLAYED);
	}

	private CarDTO newCar(long id, CarType carType) {
		return new CarDTO(id, "carName", carType, CarState.AVAILABLE);
	}
}