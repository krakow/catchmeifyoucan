package com.prodata.catchmeifyoucan.game;


import static org.assertj.core.api.Assertions.assertThat;

import com.prodata.catchmeifyoucan.car.CarDTO;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.car.domain.CarType;
import com.prodata.catchmeifyoucan.game.domain.Direction;
import com.prodata.catchmeifyoucan.game.domain.Player;
import com.prodata.catchmeifyoucan.game.domain.PlayerStatus;
import com.prodata.catchmeifyoucan.game.domain.Point;
import com.prodata.catchmeifyoucan.game.events.MotionEvent;
import com.prodata.catchmeifyoucan.game.events.PlayerJoinedEvent;
import com.prodata.catchmeifyoucan.game.events.StartGameEvent;
import com.prodata.catchmeifyoucan.map.domain.GameMap;
import com.prodata.catchmeifyoucan.map.domain.MapStatus;
import org.junit.Test;

import java.util.UUID;

public class GameAggregateTest {
	private static final String TEST_MAP =
			"0,1,0,0,0\n" +
			"0,1,0,0,0\n" +
			"0,1,1,1,1\n" +
			"0,0,0,0,1\n" +
			"1,1,1,1,1";
	private static final String TEST_MAP2 =
			"1\n," +
			"1\n," +
			"0\n," +
			"1";
	private static final String TEST_MAP3 = "1,1,0,1";

	private final UUID gameId = UUID.fromString("d5ea1ad6-ab0a-4959-81ca-f98e1e13ee7c");
	private final GameMap gameMap = newGameMap(TEST_MAP);
	private final GameMap gameMap2 = newGameMap(TEST_MAP2);
	private final GameMap gameMap3 = newGameMap(TEST_MAP3);
	private final Long player1Id = 1L;
	private final Long player2Id = 2L;
	private final CarDTO car1 = newCar(1L, CarType.MONSTER_TRUCK);


	@Test
	public void should_successfully_start_game() {
		// Given
		GameAggregate game = new GameAggregate();
		StartGameEvent startGameEvent = new StartGameEvent(gameId, gameMap);

		// When
		game.handle(startGameEvent);

		// Then
		assertThat(game.getGameEvents()).contains(startGameEvent);
		assertThat(game.getPlayers().getValues()).hasSize(0);
	}

	@Test
	public void should_successfully_join_game() {
		// Given
		GameAggregate game = createNewGameState(gameMap);
		PlayerJoinedEvent playerJoinedEvent = new PlayerJoinedEvent(gameId, player1Id, car1, newPoint(0, 4));

		// When
		game.handle(playerJoinedEvent);

		// Then
		assertThat(game.getGameEvents()).contains(playerJoinedEvent);
		assertThat(game.getPlayers().getValues()).hasSize(1);

		Player player = game.getPlayers().get(playerJoinedEvent.getPlayerId());
		assertThat(player.getDirection()).isEqualTo(Direction.NORTH);
		assertThat(player.getPosition()).isEqualTo(playerJoinedEvent.getStartingPosition());
	}

	@Test
	public void should_detect_starting_position_is_a_wall() {
		// Given
		GameAggregate game = createNewGameState(gameMap);
		PlayerJoinedEvent playerJoinedEvent = new PlayerJoinedEvent(gameId, player1Id, car1, newPoint(1,3));

		// When
		game.handle(playerJoinedEvent);

		// Then
		Player player = game.getPlayers().get(playerJoinedEvent.getPlayerId());
		assertThat(player.getStatus()).isEqualTo(PlayerStatus.UNABLE_TO_JOIN);
	}

	@Test
	public void should_be_able_to_complete_map_without_any_collision() {
		// Given
		GameAggregate game = createNewGameStateWithPlayer(gameId, player1Id, car1, newPoint(0, 4));
		Player player1 = game.getPlayers().get(player1Id);

		// When / Then
		MotionEvent firstTurnRight = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection().turnRight(), 0, false);
		game.handle(firstTurnRight);
		assertThat(player1.getDirection()).isEqualTo(Direction.EAST);

		MotionEvent moveCar4PositionsEast = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection(), 4, false);
		game.handle(moveCar4PositionsEast);
		assertThat(player1.getPosition()).isEqualTo(newPoint(4, 4));


		MotionEvent firstTurnLeft = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection().turnLeft(), 0, false);
		game.handle(firstTurnLeft);
		assertThat(player1.getDirection()).isEqualTo(Direction.NORTH);

		MotionEvent moveCar2PositionsNorth = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection(), 2, false);
		game.handle(moveCar2PositionsNorth);
		assertThat(player1.getPosition()).isEqualTo(newPoint(4, 2));

		MotionEvent secondTurnLeft = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection().turnLeft(), 0, false);
		game.handle(secondTurnLeft);
		assertThat(player1.getDirection()).isEqualTo(Direction.WEST);

		MotionEvent moveCar3PositionsWest = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection(), 3, false);
		game.handle(moveCar3PositionsWest);
		assertThat(player1.getPosition()).isEqualTo(newPoint(1, 2));

		MotionEvent secondTurnRight = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection().turnRight(), 0, false);
		game.handle(secondTurnRight);
		assertThat(player1.getDirection()).isEqualTo(Direction.NORTH);

		MotionEvent moveCar2PositionsNorthToTheFinish = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection(), 2, false);
		game.handle(moveCar2PositionsNorthToTheFinish);
		assertThat(player1.getPosition()).isEqualTo(newPoint(1, 0));

		assertThat(player1.getCar().getCarState()).isNotEqualTo(CarState.CRASHED);
	}

	@Test
	public void should_detect_wall_collision() {
		// Given
		GameAggregate game = createNewGameStateWithPlayer(gameId, player1Id, car1, newPoint(0, 4));
		Player player1 = game.getPlayers().get(player1Id);

		MotionEvent move = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection(), 1, false);

		// When
		game.handle(move);

		// Then
		assertThat(player1.getCar().getCarState()).isEqualTo(CarState.CRASHED);
	}

	@Test
	public void should_detect_wall_collision2() {
		// Given
		GameAggregate game = createNewGameState(gameMap2);
		joinPlayers(game, newPlayerJoined(player1Id, CarType.NORMAL, newPoint(0,0)));

		MotionEvent move = new MotionEvent(gameId, player1Id, car1.getId(), Direction.SOUTH, 4, false);

		// When
		game.handle(move);

		// Then
		assertThat(game.getPlayers().get(1L).getCar().getCarState()).isEqualTo(CarState.CRASHED);
		assertThat(game.getPlayers().get(1L).getPosition()).isEqualTo(new Point(0, 2));
	}


	@Test
	public void should_detect_wall_collision3() {
		// Given
		GameAggregate game = createNewGameState(gameMap3);
		joinPlayers(game, newPlayerJoined(player1Id, CarType.NORMAL, newPoint(0,0)));

		MotionEvent move = new MotionEvent(gameId, player1Id, car1.getId(), Direction.EAST, 4, false);

		// When
		game.handle(move);

		// Then
		assertThat(game.getPlayers().get(1L).getCar().getCarState()).isEqualTo(CarState.CRASHED);
		assertThat(game.getPlayers().get(1L).getPosition()).isEqualTo(new Point(2, 0));

	}

	@Test
	public void should_detect_car_collision_and_crash_two_normal_cars(){
		//Given
		GameAggregate game = createNewGameState(gameMap);
		PlayerJoinedEvent player1 = newPlayerJoined(player1Id, CarType.NORMAL, newPoint(1,2));
		PlayerJoinedEvent player2 = newPlayerJoined(player2Id, CarType.NORMAL, newPoint(1,1));
		joinPlayers(game, player1, player2);

		//When
		MotionEvent collisionMove = new MotionEvent(gameId, player1Id, 1L, player1.getDirection(), 1, false);
		game.handle(collisionMove);

		//Then
		assertThat(player1.getCar().getCarState()).isEqualTo(CarState.CRASHED);
		assertThat(player2.getCar().getCarState()).isEqualTo(CarState.CRASHED);
	}

	@Test
	public void should_detect_car_collision_and_not_crash_monster_truck(){
		//Given
		GameAggregate game = createNewGameState(gameMap);
		PlayerJoinedEvent player1 = newPlayerJoined(player1Id, CarType.MONSTER_TRUCK, newPoint(1,2));
		PlayerJoinedEvent player2 = newPlayerJoined(player2Id, CarType.NORMAL, newPoint(1,1));
		joinPlayers(game, player1, player2);

		//When
		MotionEvent collisionMove = new MotionEvent(gameId, player1Id, 1L, player1.getDirection(), 1, false);
		game.handle(collisionMove);

		//Then
		assertThat(player1.getCar().getCarState()).isEqualTo(CarState.RENTED);
		assertThat(player2.getCar().getCarState()).isEqualTo(CarState.CRASHED);
	}

	@Test
	public void should_detect_car_collision_and_not_crash_monster_truck2(){
		//Given
		GameAggregate game = createNewGameState(gameMap);
		PlayerJoinedEvent player1 = newPlayerJoined(player1Id, CarType.NORMAL, newPoint(1,2));
		PlayerJoinedEvent player2 = newPlayerJoined(player2Id, CarType.MONSTER_TRUCK, newPoint(1,1));
		joinPlayers(game, player1, player2);

		//When
		MotionEvent collisionMove = new MotionEvent(gameId, player1Id, 1L, player1.getDirection(), 1, false);
		game.handle(collisionMove);

		//Then
		assertThat(player1.getCar().getCarState()).isEqualTo(CarState.CRASHED);
		assertThat(player2.getCar().getCarState()).isEqualTo(CarState.RENTED);
	}

	@Test
	public void should_detect_wall_jump() {
		// Given
		GameAggregate game = createNewGameStateWithPlayer(gameId, player1Id, car1, newPoint(1, 4));
		Player player1 = game.getPlayers().get(player1Id);

		MotionEvent move = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection(), 2, false);
		// When
		game.handle(move);

		// Then
		assertThat(player1.getCar().getCarState()).isEqualTo(CarState.CRASHED);
	}

	@Test
	public void should_detect_player_moved_outside_the_map() {
		// Given
		GameAggregate game = createNewGameStateWithPlayer(gameId, player1Id, car1, newPoint(0, 4));
		Player player1 = game.getPlayers().get(player1Id);

		MotionEvent turnLeft = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection().turnLeft(), 0, false);
		game.handle(turnLeft);

		// When
		MotionEvent leaveMap = new MotionEvent(gameId, player1Id, car1.getId(), player1.getDirection(), 1, false);
		game.handle(leaveMap);

		// Then
		assertThat(player1.getCar().getCarState()).isEqualTo(CarState.CRASHED);
	}


	//Helpers
	private GameAggregate createNewGameState(GameMap gameMap) {
		GameAggregate game = new GameAggregate();
		StartGameEvent startGameEvent = new StartGameEvent(gameId, gameMap);
		return game.handle(startGameEvent);
	}

	private GameAggregate createNewGameStateWithPlayer(UUID gameId, Long player, CarDTO car, Point startingPosition) {
		GameAggregate game = createNewGameState(gameMap);
		PlayerJoinedEvent playerJoinedEvent = new PlayerJoinedEvent(gameId, player, car, startingPosition);
		return joinPlayers(game, playerJoinedEvent);
	}

	private GameAggregate joinPlayers(GameAggregate gameAggregate, PlayerJoinedEvent... playerJoinedEvents){
		for (PlayerJoinedEvent playerJoinedEvent : playerJoinedEvents) {
			gameAggregate.handle(playerJoinedEvent);
		}
		return gameAggregate;
	}

	private GameMap newGameMap(String board) {
		return new GameMap(1L, "TEST_MAP", board, MapStatus.IN_GAME);
	}

	private Point newPoint(int x, int y) {
		return new Point(x, y);
	}

	private CarDTO newCar(long id, CarType carType) {
		return new CarDTO(id, "carName", carType, CarState.RENTED);
	}

	private PlayerJoinedEvent newPlayerJoined(Long id, CarType carType, Point startingPoint){
		CarDTO car1 = newCar(id, carType);
		PlayerJoinedEvent playerJoined = new PlayerJoinedEvent(gameId, id, car1, startingPoint);
		return playerJoined;
	}

}