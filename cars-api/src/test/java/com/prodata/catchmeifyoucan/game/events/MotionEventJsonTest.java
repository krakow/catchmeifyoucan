package com.prodata.catchmeifyoucan.game.events;

import static org.assertj.core.api.Assertions.*;

import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.json.ObjectContent;

import com.prodata.catchmeifyoucan.TestUtils;
import com.prodata.catchmeifyoucan.game.domain.Direction;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

public class MotionEventJsonTest {
	private JacksonTester<MotionEvent> json;

	@Before
	public void setup() {
		JacksonTester.initFields(this, TestUtils.newObjectMapper());
	}

	//FIXME: this test should be split to two
	@Test
	public void should_serialize_and_deserialize_json() throws Exception {
		//Given
		MotionEvent event = new MotionEvent(UUID.randomUUID(), 1l, 1l, Direction.EAST, 1, false);

		//When
		JsonContent<MotionEvent> serialized = this.json.write(event);
		ObjectContent<MotionEvent> deserialized = this.json.parse(serialized.getJson());

		//Then
		assertThat(serialized)
			.hasJsonPathStringValue("@.id");

		assertThat(event.getId()).isEqualTo(deserialized.getObject().getId());
	}
}