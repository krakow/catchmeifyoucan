package com.prodata.catchmeifyoucan.game.events;


import static org.assertj.core.api.Assertions.assertThat;


import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.json.ObjectContent;

import com.prodata.catchmeifyoucan.TestUtils;
import com.prodata.catchmeifyoucan.car.domain.Car;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.car.domain.CarType;
import com.prodata.catchmeifyoucan.game.domain.Point;
import com.prodata.catchmeifyoucan.map.domain.GameMap;
import com.prodata.catchmeifyoucan.map.domain.MapStatus;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

public class StartGameEventJsonTest {
	private static final String TEST_MAP = "0,1,0,0,0\n0,1,0,0,0\n0,1,1,1,1\n0,0,0,0,1\n1,1,1,1,1";

	private JacksonTester<StartGameEvent> json;

	@Before
	public void setup() {
		JacksonTester.initFields(this, TestUtils.newObjectMapper());
	}

	//FIXME: this test should be split to two
	@Test
	public void should_serialize_and_deserialize_json() throws Exception {
		//Given
		Car car = newCar(1L, CarType.MONSTER_TRUCK);
		GameMap gameMap = newGameMap();
		StartGameEvent startGameEvent = new StartGameEvent(UUID.randomUUID(), gameMap);


		//When
		JsonContent<StartGameEvent> serialized = this.json.write(startGameEvent);
		ObjectContent<StartGameEvent> deserialized = this.json.parse(serialized.getJson());

		//Then
		assertThat(serialized)
			.hasJsonPathStringValue("@.id");

		assertThat(startGameEvent.getId()).isEqualTo(deserialized.getObject().getId());
	}

	private GameMap newGameMap() {
		return new GameMap(1L, "TEST_MAP", TEST_MAP, MapStatus.NEVER_PLAYED);
	}

	private Point newPoint(int x, int y) {
		return new Point(x, y);
	}

	private Car newCar(long id, CarType carType) {
		return new Car(id, "carName", carType, CarState.AVAILABLE);
	}
}