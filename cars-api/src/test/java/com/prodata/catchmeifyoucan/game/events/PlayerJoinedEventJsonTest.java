package com.prodata.catchmeifyoucan.game.events;

import static org.assertj.core.api.Assertions.assertThat;

import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.json.ObjectContent;

import com.prodata.catchmeifyoucan.TestUtils;
import com.prodata.catchmeifyoucan.car.CarDTO;
import com.prodata.catchmeifyoucan.car.domain.Car;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.car.domain.CarType;
import com.prodata.catchmeifyoucan.game.domain.Point;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

public class PlayerJoinedEventJsonTest {
	private JacksonTester<PlayerJoinedEvent> json;

	@Before
	public void setup() {
		JacksonTester.initFields(this, TestUtils.newObjectMapper());
	}

	//FIXME: this test should be split to two
	@Test
	public void should_serialize_and_deserialize_json() throws Exception {
		//Given
		PlayerJoinedEvent playerJoinedEvent = new PlayerJoinedEvent(UUID.randomUUID(), 1l, new CarDTO(1l, "", CarType.MONSTER_TRUCK, CarState.RENTED), new Point(1, 1));


		//When
		JsonContent<PlayerJoinedEvent> serialized = this.json.write(playerJoinedEvent);
		ObjectContent<PlayerJoinedEvent> deserialized = this.json.parse(serialized.getJson());

		//Then
		assertThat(serialized)
			.hasJsonPathStringValue("@.id");

		assertThat(playerJoinedEvent.getId()).isEqualTo(deserialized.getObject().getId());
	}
}