package com.prodata.catchmeifyoucan.game.events;


import static org.assertj.core.api.Assertions.assertThat;

import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.json.ObjectContent;

import com.prodata.catchmeifyoucan.TestUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

public class EndGameEventJsonTest {
	private JacksonTester<EndGameEvent> json;

	@Before
	public void setup() {
		JacksonTester.initFields(this, TestUtils.newObjectMapper());
	}

	//FIXME: this test should be split to two
	@Test
	public void should_serialize_and_deserialize_json() throws Exception {
		//Given
		EndGameEvent event = new EndGameEvent(UUID.randomUUID());

		//When
		JsonContent<EndGameEvent> serialized = this.json.write(event);
		ObjectContent<EndGameEvent> deserialized = this.json.parse(serialized.getJson());

		//Then
		assertThat(serialized)
			.hasJsonPathStringValue("@.id");

		assertThat(event.getId()).isEqualTo(deserialized.getObject().getId());
	}
}