package com.prodata.catchmeifyoucan.simulator;

import com.prodata.catchmeifyoucan.car.CarDTO;
import com.prodata.catchmeifyoucan.car.CarService;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.car.domain.CarType;
import com.prodata.catchmeifyoucan.game.GameService;
import com.prodata.catchmeifyoucan.map.MapDTO;
import com.prodata.catchmeifyoucan.map.MapService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@AllArgsConstructor
public class GameTestProvider {
    private static final String TEST_MAP =
        "0,1,0,0,0\n" +
        "0,1,0,0,0\n" +
        "0,1,1,1,1\n" +
        "0,0,0,0,1\n" +
        "1,1,1,1,1";

    @Autowired
    GameService gameService;

    @Autowired
    CarService carService;

    @Autowired
    MapService mapService;

    public UUID simulateStartGame() {
        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", TEST_MAP.getBytes());
        MapDTO mapDto = mapService.createMap("testMap", firstFile);
        return gameService.startNewGame(mapDto.getName());
    }


    public CarDTO simulateRentACar() {
        CarDTO savedCar = carService.addCar(CarDTO.builder()
                                                       .carState(CarState.AVAILABLE)
                                                       .name("Volvo")
                                                       .type(CarType.MONSTER_TRUCK)
                                                       .id(0L)
                                                       .build());

        carService.rentCar(savedCar.getId());

        return carService.findByIdAsDTO(savedCar.getId());
    }
}
