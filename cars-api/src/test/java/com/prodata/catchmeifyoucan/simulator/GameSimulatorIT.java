package com.prodata.catchmeifyoucan.simulator;

import static com.prodata.catchmeifyoucan.TestUtils.newJacksonConverter;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import com.prodata.catchmeifyoucan.Application;
import com.prodata.catchmeifyoucan.TestConfiguration;
import com.prodata.catchmeifyoucan.car.CarDTO;
import com.prodata.catchmeifyoucan.game.GameService;
import com.prodata.catchmeifyoucan.game.domain.Point;
import com.prodata.catchmeifyoucan.game.events.PlayerJoinedEvent;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Simple simulator to test websocket connection and integration with game.
 */
@ActiveProfiles("simulator")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {Application.class})
@EmbeddedKafka(controlledShutdown = true,
			   topics = {"${app.kafka.gameTopic}", "${app.kafka.gameSnapshots}", "${app.kafka.game-state-store}"})
@Import({TestConfiguration.class})
public class GameSimulatorIT {
	private static final String GAME_IN_WEBSOCKET_ENDPOINT = "/app/games-in/";
	private static String url;

	@Value("${local.server.port}")
	private int port;

	@Autowired
	GameService gameService;

	@Autowired
	GameTestProvider gameTestProvider;

	@Autowired
	TestConfiguration testConfiguration;

	private WebSocketStompClient stompClient;

	@Before
	public void setup() {
		stompClient = new WebSocketStompClient(new SockJsClient(getTransportClient()));
		stompClient.setMessageConverter(newJacksonConverter());
		url = "ws://localhost:" + port + "/games-ws";
	}

	@Ignore("issue running embedded kafka with multiple tests (https://github.com/manub/scalatest-embedded-kafka/issues/28)")
	@Test(timeout = 5_000)
	public void should_successfully_join_game() throws InterruptedException, ExecutionException, TimeoutException {
		//  Given
		UUID gameId = gameTestProvider.simulateStartGame();
		CarDTO rentedCar = gameTestProvider.simulateRentACar();

		StompSession stompSession = stompClient.connect(url, new StompSessionHandlerAdapter() {}).get(10, SECONDS);
		stompSession.subscribe(gameChannel(gameId), new GameStompFrameHandler());

		//  When
		PlayerJoinedEvent playerJoiningEvent = new PlayerJoinedEvent(gameId, 1L, rentedCar, new Point(0, 4));
		stompSession.send(GAME_IN_WEBSOCKET_ENDPOINT, playerJoiningEvent);

		//  Then
		while (testConfiguration.getJoinEventLatch().getCount() != 0) {
			TimeUnit.MILLISECONDS.sleep(500);
		}

		assertThat(testConfiguration.getJoinEventLatch().getCount()).isEqualTo(0);
	}


	private String gameChannel(UUID gameId) {
		return String.format("/topic/game_%s", gameId);
	}

	private static List<Transport> getTransportClient() {
		return new ArrayList<Transport>(1) {
			{
				add(new WebSocketTransport(new StandardWebSocketClient()));
			}
		};
	}

	private class GameStompFrameHandler implements StompFrameHandler {
		@Override
		public Type getPayloadType(StompHeaders stompHeaders) {
			return String.class;
		}

		@Override
		public void handleFrame(StompHeaders stompHeaders, Object o) {
			System.err.println("test");
		}
	}
}


