package com.prodata.catchmeifyoucan.car;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import com.prodata.catchmeifyoucan.car.domain.Car;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {
	@Mock
	private CarRepository carRepository;

	@InjectMocks
	private CarService sut;

	private static final long CAR_ID = 1;

	@Test
	public void should_remove_car(){
		long carId = 1;

		given(carRepository.existsByIdAndCarStateNot(carId, CarState.RENTED)).willReturn(true);

		assertThatCode(() ->  sut.removeCar(carId)).doesNotThrowAnyException();
	}

	@Test
	public void should_not_remove_car_and_throw_exception(){
		long carId = 1;

		given(carRepository.existsByIdAndCarStateNot(carId, CarState.RENTED)).willReturn(false);

		assertThatThrownBy(() ->  sut.removeCar(carId)).hasMessage("Car with id 1 is not in RENTED state or car not found");
	}

	@Test
	public void should_repair_car(){
		long carId = 1;
		Car carToBeRepaired = Car.builder().carState(CarState.CRASHED).id(carId).build();
		given(carRepository.findByIdAndCarState(carId, CarState.CRASHED)).willReturn(Optional.of(carToBeRepaired));
		ArgumentCaptor<Car> argumentCaptor = ArgumentCaptor.forClass(Car.class);

		assertThatCode(() ->  sut.repairCar(carId)).doesNotThrowAnyException();

		verify(carRepository, times(1)).save((argumentCaptor.capture()));
		assertThat(argumentCaptor.getValue().getCarState()).isEqualByComparingTo(CarState.AVAILABLE);
	}

	@Test
	public void should_not_repiar_car_if_car_state_available(){
		long carId = 1;
		given(carRepository.findByIdAndCarState(carId, CarState.CRASHED)).willReturn(Optional.empty());

		assertThatThrownBy(() ->  sut.repairCar(carId)).hasMessage("Car with id 1 is not in CRASHED state or car not found");

		verify(carRepository, never()).save(any(Car.class));
	}

	@Test
	public void should_rent_car(){
		long carId = 1;
		Car carToBeRented = Car.builder().carState(CarState.AVAILABLE).id(carId).build();
		given(carRepository.findByIdAndCarState(carId, CarState.AVAILABLE)).willReturn(Optional.of(carToBeRented));
		ArgumentCaptor<Car> argumentCaptor = ArgumentCaptor.forClass(Car.class);

		assertThatCode(() ->  sut.rentCar(carId)).doesNotThrowAnyException();

		verify(carRepository, times(1)).save((argumentCaptor.capture()));

		assertThat(argumentCaptor.getValue().getCarState()).isEqualByComparingTo(CarState.RENTED);
	}

	@Test
	public void should_not_rent_car_if_state_not_available(){
		long carId = 1;

		given(carRepository.findByIdAndCarState(carId, CarState.AVAILABLE)).willReturn(Optional.empty());

		assertThatThrownBy(() ->  sut.rentCar(carId)).hasMessage("Car with id 1 is not in AVAILABLE state or car not found");

		verify(carRepository, never()).save(any(Car.class));
	}

	@Test
	public void should_return_car(){
		long carId = 1;
		Car carToBeReleased = Car.builder().carState(CarState.RENTED).id(carId).build();
		given(carRepository.findByIdAndCarState(carId, CarState.RENTED)).willReturn(Optional.of(carToBeReleased));
		ArgumentCaptor<Car> argumentCaptor = ArgumentCaptor.forClass(Car.class);

		assertThatCode(() ->  sut.returnCar(carId)).doesNotThrowAnyException();

		verify(carRepository, times(1)).save((argumentCaptor.capture()));
		assertThat(argumentCaptor.getValue().getCarState()).isEqualByComparingTo(CarState.AVAILABLE);
	}

	@Test
	public void should_not_return_car_if_state_not_rented(){
		given(carRepository.findByIdAndCarState(CAR_ID, CarState.RENTED)).willReturn(Optional.empty());

		assertThatThrownBy(() ->  sut.returnCar(CAR_ID)).hasMessage("Car with id 1 is not in RENTED state or car not found");

		verify(carRepository, never()).save(any(Car.class));
	}

	@Test
	public void should_throw_exception_if_not_car_found(){
		assertThatThrownBy(() -> sut.findById(CAR_ID)).hasMessage("Car with id 1 does not exist");
	}
}