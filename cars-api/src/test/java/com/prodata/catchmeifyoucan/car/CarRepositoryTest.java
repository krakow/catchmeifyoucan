package com.prodata.catchmeifyoucan.car;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.prodata.catchmeifyoucan.car.domain.Car;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.car.domain.CarType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@DataJpaTest
@ActiveProfiles("repositoryTest")
public class CarRepositoryTest {

	@Autowired
	CarRepository carRepository;

	@Before
	public void initializeDB() {
			List<Car> cars = prepareTestData();
			carRepository.saveAll(cars);
	}

	@Test
	public void shouldReturnCarWithAvailableStatusAsDTO(){
		List<CarDTO> allByCarStateAsDTO = carRepository.findAllByCarStateAsDTO(CarState.AVAILABLE);
		assertThat(allByCarStateAsDTO).extracting("name").contains("car2", "car3");
	}

	@Test
	public void shouldReturnCarAsDTO(){
		CarDTO carDTO = carRepository.findByIdAsDTO(2L).get();
		assertThat(carDTO).extracting("name").contains("car2");
	}

	@Test
	public void shouldReturnCar(){
		Car car = carRepository.findByIdAndCarState(2L, CarState.AVAILABLE).get();
		assertThat(car).extracting("name").contains("car2");
	}

	@Test
	public void shouldNotFindCarWithId4InStateNotAvailable(){
		boolean result = carRepository.existsByIdAndCarStateNot(3L, CarState.AVAILABLE);
		assertThat(result).isFalse();
	}

	private List<Car> prepareTestData(){
		List<Car> cars = new ArrayList<>();
		cars.add(new Car(1L,"car1", CarType.MONSTER_TRUCK, CarState.CRASHED));
		cars.add(new Car(2L,"car2", CarType.NORMAL, CarState.AVAILABLE));
		cars.add(new Car(3L,"car3", CarType.RACER, CarState.AVAILABLE));

		return cars;
	}
 }