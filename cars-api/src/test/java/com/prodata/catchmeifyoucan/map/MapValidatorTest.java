package com.prodata.catchmeifyoucan.map;


import static org.assertj.core.api.Java6Assertions.assertThatCode;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;

import com.prodata.catchmeifyoucan.map.MapValidator;
import org.junit.Test;

public class MapValidatorTest {

	private static final String EXPECTED_MAP_IS_IMPASSABLE = "Map is impassable";
	private static final String EXPECTED_INVALID_DELIMITER_USED = "Invalid delimiter has been used: For input string: \"1;\"";
	private static final String EXPECTED_MAP_IS_NOT_SQUARE_MATRIX = "Map is not square matrix";
	private static final String EXPECTED_EMPTY_MAP_CONTENT_RECEIVED = "Empty mapContent received";

	@Test
	public void should_throw_exception_when_received_empty_map_content(){
		String map = " ";
		assertThatThrownBy(() ->  MapValidator.validateMap(map)).hasMessage(EXPECTED_EMPTY_MAP_CONTENT_RECEIVED);
	}

	@Test
	public void should_throw_exception_when_invalid_delimter_used_in_map(){
		String map =
			"0,1;\n" +
			"1,1";
		assertThatThrownBy(() ->  MapValidator.validateMap(map))
			.hasMessage(EXPECTED_INVALID_DELIMITER_USED);
	}

	@Test
	public void should_throw_exception_when_map_is_not_square_matrix(){
		String map =
			"1,1,1,1,0\n" +
			"0,1,1,0,0";
		assertThatThrownBy(() ->  MapValidator.validateMap(map)).hasMessage(EXPECTED_MAP_IS_NOT_SQUARE_MATRIX);
	}

	@Test
	public void should_throw_exception_when_map_is_impassable(){
		String map =
			"0,0,0,0,1\n" +
			"0,0,1,0,0\n" +
			"1,0,1,0,0\n" +
			"1,1,0,0,0\n" +
			"1,0,0,1,1";
		assertThatThrownBy(() ->  MapValidator.validateMap(map)).hasMessage(EXPECTED_MAP_IS_IMPASSABLE);
	}


	@Test
	public void should_throw_exception_when_map_is_impassable_2(){
		String map =
			"0,0,0,0,0\n" +
			"0,0,0,0,0\n" +
			"0,0,0,0,0\n" +
			"0,0,0,0,0\n" +
			"0,0,0,0,0";
		assertThatThrownBy(() ->  MapValidator.validateMap(map)).hasMessage(EXPECTED_MAP_IS_IMPASSABLE);

	}

	@Test
	public void should_throw_exception_when_map_is_impassable_3(){
		String map =
			"0,0,0,0,1\n" +
			"0,0,1,0,0\n" +
			"1,0,1,0,0\n" +
			"1,1,0,0,0\n" +
			"1,0,0,1,1";
		assertThatThrownBy(() ->  MapValidator.validateMap(map)).hasMessage(EXPECTED_MAP_IS_IMPASSABLE);
	}

	@Test
	public void should_Not_throw_any_exception_when_map_is_valid(){
		String map =
			"0,0,1,1,1\n" +
			"1,0,1,0,1\n" +
			"1,1,1,0,1\n" +
			"1,0,1,1,0\n" +
			"1,0,0,1,1";
		assertThatCode(() ->  MapValidator.validateMap(map)).doesNotThrowAnyException();
	}

	@Test
	public void should_Not_throw_any_exception_when_map_is_valid_2(){
		String map =
			"0,0,0,0,0\n" +
			"0,0,1,0,0\n" +
			"0,0,1,0,0\n" +
			"0,0,1,0,0\n" +
			"0,0,0,0,0";
		assertThatCode(() ->  MapValidator.validateMap(map)).doesNotThrowAnyException();
	}
}