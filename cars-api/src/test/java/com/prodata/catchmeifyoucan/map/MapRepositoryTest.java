package com.prodata.catchmeifyoucan.map;

import static com.google.common.collect.Sets.newHashSet;
import static com.prodata.catchmeifyoucan.map.domain.MapStatus.AVAILABLE;
import static com.prodata.catchmeifyoucan.map.domain.MapStatus.NEVER_PLAYED;
import static org.assertj.core.api.Java6Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.prodata.catchmeifyoucan.map.domain.GameMap;
import com.prodata.catchmeifyoucan.map.domain.MapStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@DataJpaTest
@ActiveProfiles("repositoryTest")
public class MapRepositoryTest {

	@Autowired
	MapRepository sut;

	@Before
	public void initializeDB() {
		List<GameMap> maps = prepareTestData();
		sut.saveAll(maps);
	}

	@Test
	public void should_find_free_map_1(){
		GameMap map1 = sut.findByNameAndMapStatusIn("map1", newHashSet(NEVER_PLAYED, AVAILABLE)).get();
		assertThat(map1).extracting("name").contains("map1");
	}

	@Test
	public void should_find_map1(){
		GameMap map1 = sut.findByNameAndMapStatusNot("map1", MapStatus.IN_GAME).get();
		assertThat(map1).extracting("name").contains("map1");

	}

	@Test
	public void should_not(){
		assertThat(sut.existsByNameAndMapStatusNot("map1", MapStatus.IN_GAME)).isTrue();
	}

	private List<GameMap> prepareTestData() {
		List<GameMap> gameMaps = new ArrayList<>();
		gameMaps.add(GameMap.builder()
		                         .name("map1")
		                         .mapStatus(AVAILABLE)
	                             .map("map")
		                         .build());
		gameMaps.add(GameMap.builder()
		                         .name("map2")
		                         .map("map")
		                         .mapStatus(NEVER_PLAYED)
		                         .build());
		return gameMaps;
	}
}