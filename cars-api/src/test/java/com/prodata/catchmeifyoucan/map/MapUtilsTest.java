package com.prodata.catchmeifyoucan.map;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class MapUtilsTest {

	@Test
	public void should_parse_valid_map() {
		// Given
		String map = "1,1,1,0,0\n1,1,0,0,0\n1,1,1,1,1\n1,0,1,0,1\n1,1,1,1,1";

		// When
		Integer[][] parsedMap = MapUtils.parseMap(map);

		// Then
		assertThat(parsedMap[0][0]).isEqualTo(1);
		assertThat(parsedMap[0][1]).isEqualTo(1);
		assertThat(parsedMap[0][2]).isEqualTo(1);
		assertThat(parsedMap[0][3]).isEqualTo(0);
		assertThat(parsedMap[0][4]).isEqualTo(0);

		assertThat(parsedMap[4][0]).isEqualTo(1);
		assertThat(parsedMap[4][1]).isEqualTo(1);
		assertThat(parsedMap[4][2]).isEqualTo(1);
		assertThat(parsedMap[4][3]).isEqualTo(1);
		assertThat(parsedMap[4][4]).isEqualTo(1);
	}
}