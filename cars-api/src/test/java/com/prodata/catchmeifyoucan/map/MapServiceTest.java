package com.prodata.catchmeifyoucan.map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.springframework.mock.web.MockMultipartFile;

import com.google.common.collect.Sets;
import com.prodata.catchmeifyoucan.map.domain.GameMap;
import com.prodata.catchmeifyoucan.map.domain.MapStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MapServiceTest {

	private  static final String MAP_NAME = "map1";
	private  static final String MAP_BODY =
			"0,0,1,1,1\n" +
			"0,0,1,0,0\n" +
			"1,0,1,0,0\n" +
			"1,1,1,0,0\n" +
			"1,0,1,1,1";

	@InjectMocks
	private MapService sut;

	@Mock
	private MapRepository  mapRepository;


	@Test
	public void should_mark_free_map_as_deleted(){
		GameMap gameMap = createMap(MapStatus.AVAILABLE);
		given(mapRepository.findByNameAndMapStatusNot(MAP_NAME, MapStatus.IN_GAME)).willReturn(Optional.of(gameMap));
		ArgumentCaptor<GameMap> argumentCaptor = ArgumentCaptor.forClass(GameMap.class);

		assertThatCode(() ->  sut.deleteMap(MAP_NAME)).doesNotThrowAnyException();

		verify(mapRepository, times(1)).save((argumentCaptor.capture()));

		assertThat(argumentCaptor.getValue().getMapStatus()).isEqualByComparingTo(MapStatus.DELETED);
	}

	@Test
	public void should_delete_never_played_map(){
		String mapName = MAP_NAME;
		GameMap gameMap = createMap(MapStatus.NEVER_PLAYED);
		given(mapRepository.findByNameAndMapStatusNot(mapName, MapStatus.IN_GAME)).willReturn(Optional.of(gameMap));

		assertThatCode(() ->  sut.deleteMap(mapName)).doesNotThrowAnyException();

		verify(mapRepository, times(1)).delete((gameMap));
	}

	@Test
	public void should_throw_exception_when_deleting_already_marked_as_deleted(){
		String mapName = MAP_NAME;
		GameMap gameMap = createMap(MapStatus.DELETED);
		given(mapRepository.findByNameAndMapStatusNot(mapName, MapStatus.IN_GAME)).willReturn(Optional.of(gameMap));

		assertThatThrownBy(() ->  sut.deleteMap(mapName)).hasMessage("Map with map1 id has been marked as Deleted before");

		verify(mapRepository, never()).delete(any(GameMap.class));
		verify(mapRepository, never()).save(any(GameMap.class));
	}

	@Test
	public void should_create_map(){
		GameMap expectedMap = createMap(MapStatus.NEVER_PLAYED);
		expectedMap.setId(null);
		given(mapRepository.save(any(GameMap.class))).willReturn(expectedMap);

		assertThatCode(()->sut.createMap("map1", new MockMultipartFile("map1", MAP_BODY.getBytes()))).doesNotThrowAnyException();

		verify(mapRepository, times(1)).save(expectedMap);
	}

	@Test
	public void should_not_create_map_if_other_not_deleted_map_uses_name(){
		given(mapRepository.existsByNameAndMapStatusNot(MAP_NAME, MapStatus.DELETED)).willReturn(true);

		assertThatThrownBy(()->sut.createMap("map1", new MockMultipartFile("map1", MAP_BODY.getBytes()))).hasMessageContaining("The 'map1' name is already used by active map");

		verify(mapRepository,never()).save(any(GameMap.class));
	}

	@Test
	public void should_make_map_available(){
		GameMap gameMap = createMap(MapStatus.IN_GAME);

		given(mapRepository.findById(1L)).willReturn(Optional.of(gameMap));

		GameMap expectedResult = sut.makeMapAvailable(1L);

		verify(mapRepository,times(1)).save(expectedResult);
	}

	@Test
	public void should_load_map_to_game(){
		GameMap gameMap = createMap(MapStatus.NEVER_PLAYED);
		given(mapRepository.findByNameAndMapStatusIn(MAP_NAME, Sets.newHashSet(MapStatus.NEVER_PLAYED, MapStatus.AVAILABLE))).willReturn(Optional.of(gameMap));

		GameMap loadedMap = sut.loadMapToGame(MAP_NAME);

		assertThat(loadedMap.getMapStatus()).isEqualByComparingTo(MapStatus.IN_GAME);
		verify(mapRepository, times(1)).save(loadedMap);
	}

	@Test
	public void should_throw_exception_if_map_to_load_already_in_game(){
		given(mapRepository.findByNameAndMapStatusIn(MAP_NAME, Sets.newHashSet(MapStatus.NEVER_PLAYED, MapStatus.AVAILABLE))).willReturn(Optional.empty());

		assertThatThrownBy(() -> sut.loadMapToGame(MAP_NAME)).hasMessageContaining("");

		verify(mapRepository, never()).save(any(GameMap.class));
	}

	private GameMap createMap(MapStatus mapStatus) {
		return GameMap.builder().name(MAP_NAME).id(1L).map(MAP_BODY).mapStatus(mapStatus).build();
	}
}