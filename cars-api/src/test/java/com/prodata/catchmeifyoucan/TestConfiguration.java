package com.prodata.catchmeifyoucan;

import com.prodata.catchmeifyoucan.game.GameAggregate;
import com.prodata.catchmeifyoucan.game.GameAuditListener;
import com.prodata.catchmeifyoucan.game.events.EndGameEvent;
import com.prodata.catchmeifyoucan.game.events.GameEvent;
import com.prodata.catchmeifyoucan.game.events.MotionEvent;
import com.prodata.catchmeifyoucan.game.events.StartGameEvent;
import lombok.AllArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.concurrent.CountDownLatch;

@Configuration
@AllArgsConstructor
public class TestConfiguration {

    @Autowired
    GameAuditListener gameAuditListener;

    private final CountDownLatch motionLatch = new CountDownLatch(1);
    private final CountDownLatch startGameLatch = new CountDownLatch(1);
    private final CountDownLatch endGameLatch = new CountDownLatch(1);
    private final CountDownLatch joinEventLatch = new CountDownLatch(1);

    @KafkaListener(topics = "${app.kafka.gameTopic}", containerFactory = "gameEventsListenerFactory",
            groupId = "${app.kafka.auditGroupId}")
    public void listenRawGameEvents(ConsumerRecord<String, GameEvent> cr) {
        gameAuditListener.listenRawGameEvents(cr);
        if (cr.value().getType().equals(MotionEvent.class.getSimpleName())) {
            motionLatch.countDown();
        } else if (cr.value().getType().equals(EndGameEvent.class.getSimpleName())) {
            endGameLatch.countDown();
        } else if(cr.value().getType().equals(StartGameEvent.class.getSimpleName())) {
            startGameLatch.countDown();
        } else {
            joinEventLatch.countDown();
        }
    }

    @KafkaListener(topics = "${app.kafka.gameSnapshots}", containerFactory = "gameAuditKafkaListenerFactory",
            groupId = "${app.kafka.auditGroupId}")
    public void listenGameSnapshotTopic(ConsumerRecord<String, GameAggregate> cr) {
        gameAuditListener.listenGameSnapshotTopic(cr);
    }

    public CountDownLatch getEndGameLatch() {
        return endGameLatch;
    }

    public CountDownLatch getMotionLatch() {
        return motionLatch;
    }

    public CountDownLatch getStartGameLatch() {
        return startGameLatch;
    }

    public CountDownLatch getJoinEventLatch() {
        return joinEventLatch;
    }
}