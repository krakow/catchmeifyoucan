Feature: Car actions in a game
#    Each car has its type. The car should be identified by a name.
#    One car can be placed only in one game.

  Scenario: Putting cars ino the game
    Given game has already been started
    Given there are non crashed cars available for the game (not attending in the other game)
    When user try to put car int a game with valid starting points
    Then car should appear in the game

