package cucumber.steps;

import com.prodata.catchmeifyoucan.Application;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ContextConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {Application.class})
@EmbeddedKafka(controlledShutdown = true,
        topics = {"${app.kafka.gameTopic}", "${app.kafka.gameSnapshots}", "${app.kafka.game-state-store}"})
@Import({TestConfiguration.class})
@ActiveProfiles("simulator")
public @interface CucumberStepsDefinition {
}