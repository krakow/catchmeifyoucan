package cucumber.steps.car.inthegame;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(format = {"pretty", "html:target/cucumber/car_in_the_game/", "junit:target/cucumber/car_in_the_game.xml"},
        features = {
                "src/cucumberTest/resources/features/car_in_the_game.feature"
        })
public class CarInTheGameAT {

}
