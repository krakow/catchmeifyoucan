package cucumber.steps.car.inthegame;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.prodata.catchmeifyoucan.car.CarDTO;
import com.prodata.catchmeifyoucan.car.CarService;
import com.prodata.catchmeifyoucan.car.domain.CarState;
import com.prodata.catchmeifyoucan.car.domain.CarType;
import com.prodata.catchmeifyoucan.game.GameService;
import com.prodata.catchmeifyoucan.game.domain.Direction;
import com.prodata.catchmeifyoucan.game.domain.Point;
import com.prodata.catchmeifyoucan.game.events.MotionEvent;
import com.prodata.catchmeifyoucan.game.events.PlayerJoinedEvent;
import com.prodata.catchmeifyoucan.map.MapDTO;
import com.prodata.catchmeifyoucan.map.MapService;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.steps.CucumberStepsDefinition;
import cucumber.steps.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;


@CucumberStepsDefinition
public class CarInTheGameStepDefs {

    private static final String GAME_IN_WEBSOCKET_ENDPOINT = "/app/games-in/";
    private static String url;

    @Value("${local.server.port}")
    private int port;

    @Autowired
    GameService gameService;

    @Autowired
    MapService mapService;

    @Autowired
    CarService carService;

    @Autowired
    TestConfiguration testConfiguration;

    private WebSocketStompClient stompClient;

    private UUID gameId;

    private CarDTO savedCar;

    @Before
    public void setup() {
        stompClient = new WebSocketStompClient(new SockJsClient(getTransportClient()));
        stompClient.setMessageConverter(newJacksonConverter());
        url = "ws://localhost:" + port + "/games-ws";
    }

    @Autowired
    TestConfiguration config;

    @Given("^game has already been started$")
    public void game_has_already_been_started()throws Throwable {
        gameId = startTestGameWithDefaultMap();
    }

    @Given("^there are non crashed cars available for the game \\(not attending in the other game\\)$")
    public void there_are_non_crashed_cars_available_for_the_game()throws Throwable {
        savedCar = carService.addCar(CarDTO.builder()
                .carState(CarState.AVAILABLE)
                .name("Volvo")
                .type(CarType.MONSTER_TRUCK)
                .id(0L)
                .build());
        carService.rentCar(savedCar.getId());
    }


    @When("^user try to put car int a game with valid starting points$")
    public void user_try_to_put_car_int_a_game_with_valid_starting()throws Throwable {
        CarDTO rentedCar = carService.findByIdAsDTO(savedCar.getId());

        StompSession stompSession = stompClient.connect(url, new StompSessionHandlerAdapter() {}).get(10, SECONDS);
        stompSession.subscribe(gameChannel(gameId), new GameStompFrameHandler());

        PlayerJoinedEvent playerJoiningEvent = new PlayerJoinedEvent(gameId, 1L, rentedCar, new Point(0, 4));
        stompSession.send(GAME_IN_WEBSOCKET_ENDPOINT, playerJoiningEvent);
//        MotionEvent motionEvent = new MotionEvent(gameId, 1L, rentedCar.getId(), Direction.NORTH, 1, false);
//        stompSession.send(GAME_IN_WEBSOCKET_ENDPOINT, motionEvent);
    }

    @Then("^car should appear in the game$")
    public void car_should_appear_in_the_game$()throws Throwable {
        while (testConfiguration.getJoinEventLatch().getCount() != 0) {
            TimeUnit.MILLISECONDS.sleep(500);
        }
        assertThat(testConfiguration.getJoinEventLatch().getCount()).isEqualTo(0);
    }


    public UUID startTestGameWithDefaultMap() {
        String map =
                        "0,0,0,0,0\n" +
                        "0,0,1,0,0\n" +
                        "0,0,1,0,0\n" +
                        "0,0,1,0,0\n" +
                        "0,0,0,0,0";

        MockMultipartFile firstFile = new MockMultipartFile("data", "filename.txt", "text/plain", map.getBytes());
        MapDTO mapDto = mapService.createMap("testMap", firstFile);
        return gameService.startNewGame(mapDto.getName());
    }

    private String gameChannel(UUID gameId) {
        return String.format("/topic/game_%s", gameId);
    }

    private static List<Transport> getTransportClient() {
        return new ArrayList<Transport>(1) {
            {
                add(new WebSocketTransport(new StandardWebSocketClient()));
            }
        };
    }

    private class GameStompFrameHandler implements StompFrameHandler {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return String.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            System.err.println("test");
        }
    }

    private MessageConverter newJacksonConverter() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        MappingJackson2MessageConverter mappingJackson2MessageConverter = new MappingJackson2MessageConverter();
        mappingJackson2MessageConverter.setObjectMapper(objectMapper);
        return  mappingJackson2MessageConverter;
    }

}