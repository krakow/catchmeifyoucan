# Introduction

CatchMeIfYouCan application.

## Prerequisites (Intelij Idea)

Java 8.

Install Lombok plugin (Plugins -> Browse repository... -> Lombok ).

Enable annotation processors: Preferences -> Build, Execution, Deployment -> Compiler -> Annotation Processors -> Enable annotation processing

### 1) Run cars-api application

Follow instructions attached [here](./cars-api/README.md).

### 2) Run cars-web application

| *Tool* | *Description* |
| ------------- | ------------- |
| console | npm start |



